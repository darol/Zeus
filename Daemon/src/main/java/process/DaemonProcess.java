package process;

import component.Process;
import logger.LoggerProvider;
import component.Nameable;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.util.concurrent.*;

public abstract class DaemonProcess extends Process implements Nameable {
    protected abstract void processCycle();
    protected abstract String getProcessExtraInfo();
    private DateTime startTime;

    @Override
    protected long getInitialDelayMillis() {
        return 0;
    }

    @Override
    protected Runnable getRunnable() {
        return () -> {
            cycleStart();
            processCycle();
            cycleEnd();
        };
    }

    @Override
    public void start() {
        super.start();
        LoggerProvider.daemon().info(String.format("Starting %s", getProcessInfo()));
    }

    private void cycleStart(){
        startTime = DateTime.now();
    }

    private void cycleEnd() {
        try {
            LoggerProvider.daemon().info(String.format("%s cycle finished in %d seconds. Next cycle in %d seconds",
                    getProcessInfo(),
                    Seconds.secondsBetween(startTime, DateTime.now()).getSeconds(),
                    TimeUnit.MILLISECONDS.toSeconds(getDelayMillis())));
            Thread.sleep(getDelayMillis());
        } catch (InterruptedException e) {
            LoggerProvider.daemon().info(String.format("%s sleep interrupted", getSimpleName()));
        }
    }

    private String getProcessInfo(){
        return String.format("%s%s", getSimpleName(), getProcessExtraInfo());
    }

    protected void logEntityUpdate(Class entityClass, int saved, int deleted, String extraMessage){
        LoggerProvider.daemon().info(String.format("%s %-20s saved%4d, deleted%4d %s", getSimpleName(), entityClass.getSimpleName(), saved, deleted, extraMessage));
    }
}