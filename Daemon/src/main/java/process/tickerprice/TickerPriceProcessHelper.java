package process.tickerprice;

import client.ExchangeClient;
import process.ProcessHelper;
import hibernate.EntityPersister;
import hibernate.entity.TickerPriceEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

public class TickerPriceProcessHelper extends ProcessHelper {
    public TickerPriceProcessHelper(ExchangeClient exchangeClient, EntityPersister entityPersister) {
        super(exchangeClient, entityPersister);
    }

    List<TickerPriceEntity> getAllEnabledPrices() {
        return exchangeClient.getAllPrices().stream()
                .filter(tickerPrice -> getEnabledSymbols().contains(tickerPrice.getSymbol()))
                .collect(Collectors.toList());
    }

    List<TickerPriceEntity> getOldTickerpriceEntities(Timestamp oldTimeStamp){
        return entityPersister.getAllEntities(TickerPriceEntity.class).stream()
                .filter(tickerpriceEntity -> tickerpriceEntity.getTime().before(oldTimeStamp))
                .collect(Collectors.toList());
    }
}
