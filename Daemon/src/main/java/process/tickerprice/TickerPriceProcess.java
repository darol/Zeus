package process.tickerprice;

import config.DaemonConfig;
import process.DaemonProcess;
import hibernate.entity.TickerPriceEntity;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.List;

public class TickerPriceProcess extends DaemonProcess {
    private static final long MAX_TICKER_PRICE_AGE_MILLIS = DaemonConfig.getInstance().getMaxTickerPriceAgeMillis();
    private static final long TICKER_PRICE_PROCESS_CYCLE_DURATION = DaemonConfig.getInstance().getTickerPriceProcessCycleDurationMillis();
    private final TickerPriceProcessHelper tickerPriceProcessHelper;

    public TickerPriceProcess(TickerPriceProcessHelper tickerPriceProcessHelper) {
        this.tickerPriceProcessHelper = tickerPriceProcessHelper;
    }

    @Override
    protected long getDelayMillis() {
        return TICKER_PRICE_PROCESS_CYCLE_DURATION;
    }

    @Override
    protected void processCycle() {
        List<TickerPriceEntity> newTickerPriceEntities = tickerPriceProcessHelper.getAllEnabledPrices();
        List<TickerPriceEntity> oldTickerPriceEntities = tickerPriceProcessHelper.getOldTickerpriceEntities(getOldTimestamp());

        tickerPriceProcessHelper.saveEntities(newTickerPriceEntities);
        tickerPriceProcessHelper.deleteEntities(oldTickerPriceEntities);

        logEntityUpdate(TickerPriceEntity.class, newTickerPriceEntities.size(), oldTickerPriceEntities.size(), "");
    }

    @Override
    protected String getProcessExtraInfo() {
        return "";
    }

    private Timestamp getOldTimestamp(){
        return new Timestamp(DateTime.now().getMillis() - MAX_TICKER_PRICE_AGE_MILLIS);
    }
}
