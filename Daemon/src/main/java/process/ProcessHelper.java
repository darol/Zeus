package process;

import client.ExchangeClient;
import config.DataBaseConfig;
import hibernate.EntityPersister;
import hibernate.entity.TickerEnabledEntity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class ProcessHelper {
    protected final ExchangeClient exchangeClient;
    protected final EntityPersister entityPersister;
    private final List<String> enabledSymbols;

    public ProcessHelper(ExchangeClient exchangeClient, EntityPersister entityPersister) {
        this.exchangeClient = exchangeClient;
        this.entityPersister = entityPersister;
        enabledSymbols = getEnabledSymbols(entityPersister);
    }

    public <T> void saveEntities(Collection<T> entities){
        entityPersister.save(entities);
    }

    public <T> void deleteEntities(Collection<T> entities){
        entityPersister.delete(entities);
    }

    protected List<String> getEnabledSymbols() {
        return enabledSymbols;
    }

    private List<String> getEnabledSymbols(EntityPersister entityPersister){
        return entityPersister.getAllEntities(TickerEnabledEntity.class).stream()
                .filter(tickerEnabledEntity -> DataBaseConfig.getTrueString().equals(tickerEnabledEntity.getEnabled()))
                .map(TickerEnabledEntity::getSymbol)
                .collect(Collectors.toList());
    }
}
