package process.candlestick;

import component.interval.Interval;
import component.symbol.SymbolInterval;
import process.DaemonProcess;
import hibernate.entity.CandlestickEntity;
import org.joda.time.DateTime;

import java.util.List;

public class CandlestickProcess extends DaemonProcess {
    private final CandlestickProcessHelper candlestickProcessHelper;
    private final List<String> sortedEnabledSymbols;
    private final List<Interval> intervals;
    private final long delayMillis;

    public CandlestickProcess(CandlestickProcessHelper candlestickProcessHelper, List<Interval> intervals, long delayMillis) {
        this.candlestickProcessHelper = candlestickProcessHelper;
        this.intervals = intervals;
        this.delayMillis = delayMillis;

        sortedEnabledSymbols = candlestickProcessHelper.getSortedEnabledSymbols();
    }

    @Override
    protected void processCycle() {
        intervals.forEach(candlestickInterval -> sortedEnabledSymbols
                .forEach(symbol -> processUpdate(new SymbolInterval(symbol, candlestickInterval))));
    }

    @Override
    protected String getProcessExtraInfo() {
        return intervals.toString();
    }

    private void processUpdate(SymbolInterval symbolInterval) {
        if(isActive()){
            long nowMillis = DateTime.now().getMillis();
            long intervalMillis = symbolInterval.getInterval().getMillis();
            long lastCloseTimeFromCache = candlestickProcessHelper.getLastCloseTimeFromCache(symbolInterval);

            if(isUpdateNeeded(nowMillis, intervalMillis, lastCloseTimeFromCache)){
                updateCandlesticks(symbolInterval, nowMillis, intervalMillis, lastCloseTimeFromCache);
            }
        }
    }

    private boolean isUpdateNeeded(long nowMillis, long intervalMillis, long lastCloseTime) {
        return (lastCloseTime + intervalMillis) <= nowMillis;
    }

    private void updateCandlesticks(SymbolInterval symbolInterval, long nowMillis, long intervalMillis, long lastCloseTimeFromCache) {
        long limitedLastCloseTime = candlestickProcessHelper.getLimitedLastCloseTime(lastCloseTimeFromCache, nowMillis, intervalMillis);

        List<CandlestickEntity> newCandlesticks = candlestickProcessHelper.getNewCandlesticks(symbolInterval, limitedLastCloseTime, nowMillis);
        List<CandlestickEntity> oldCandlesticks = candlestickProcessHelper.getOldCandlesticksFromCache(symbolInterval, nowMillis, intervalMillis);

        candlestickProcessHelper.removeOldCandlesticksFromCache(symbolInterval, oldCandlesticks);
        candlestickProcessHelper.addNewCandlesticksToCache(symbolInterval, newCandlesticks);

        candlestickProcessHelper.deleteEntities(oldCandlesticks);
        candlestickProcessHelper.saveEntities(newCandlesticks);

        logEntityUpdate(CandlestickEntity.class, newCandlesticks.size(), oldCandlesticks.size(), symbolInterval.toString());
    }

    @Override
    protected long getDelayMillis() {
        return delayMillis;
    }
}
