package process.candlestick;

import component.interval.Interval;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public enum CandlestickProcessInterval {
    ONE_MINUTE(Interval.ONE_MINUTE, CandlestickProcessType.ONE_MINUTE),

    FIVE_MINUTES(Interval.FIVE_MINUTES, CandlestickProcessType.FIVE_MINUTE),
    FIFTEEN_MINUTES(Interval.FIFTEEN_MINUTES, CandlestickProcessType.FIVE_MINUTE),

    HALF_HOURLY(Interval.HALF_HOURLY, CandlestickProcessType.HOUR),
    HOURLY(Interval.HOURLY, CandlestickProcessType.HOUR),
    FOUR_HOURLY(Interval.FOUR_HOURLY, CandlestickProcessType.HOUR),
    TWELVE_HOURLY(Interval.TWELVE_HOURLY, CandlestickProcessType.HOUR),

    DAILY(Interval.DAILY, CandlestickProcessType.DAY),
    THREE_DAILY(Interval.THREE_DAILY, CandlestickProcessType.DAY),
    WEEKLY(Interval.WEEKLY, CandlestickProcessType.DAY),
    MONTHLY(Interval.MONTHLY, CandlestickProcessType.DAY);

    private final Interval interval;
    private final CandlestickProcessType candlestickProcessType;

    CandlestickProcessInterval(Interval interval, CandlestickProcessType candlestickProcessType) {
        this.interval = interval;
        this.candlestickProcessType = candlestickProcessType;
    }

    public Interval getInterval() {
        return interval;
    }

    public CandlestickProcessType getCandlestickProcessType() {
        return candlestickProcessType;
    }

    public static long getNumberOfProcesses(){
        return Arrays.stream(values()).map(CandlestickProcessInterval::getCandlestickProcessType).distinct().count();
    }

    public static List<Interval> getIntervalsByProcessType(CandlestickProcessType candlestickProcessType){
        return Arrays.stream(values())
                .filter(candlestickProcessInterval -> candlestickProcessInterval.candlestickProcessType.equals(candlestickProcessType))
                .map(candlestickProcessInterval -> candlestickProcessInterval.interval)
                .collect(Collectors.toList());
    }

    public enum CandlestickProcessType {
        ONE_MINUTE(TimeUnit.MINUTES.toMillis(1)),
        FIVE_MINUTE(TimeUnit.MINUTES.toMillis(5)),
        HOUR(TimeUnit.MINUTES.toMillis(30)),
        DAY(TimeUnit.HOURS.toMillis(12));

        private final long delayMillis;

        CandlestickProcessType(long delayMillis) {
            this.delayMillis = delayMillis;
        }

        public long getDelayMillis() {
            return delayMillis;
        }
    }
}