package process.candlestick;

import hibernate.entity.TickerStatisticsEntity;
import helper.ParsingHelper;

import java.util.Comparator;

public enum TickerStatisticEntityComparator {
    VOLUME((o1, o2) -> Double.compare(getVolume(o2), getVolume(o1))),
    CHANGE_PERCENT((o1, o2) -> Double.compare(getChange(o2), getChange(o1)));

    private final Comparator<TickerStatisticsEntity> comparator;

    TickerStatisticEntityComparator(Comparator<TickerStatisticsEntity> comparator) {
        this.comparator = comparator;
    }

    public Comparator<TickerStatisticsEntity> getComparator() {
        return comparator;
    }

    private static double getVolume(TickerStatisticsEntity tickerStatisticsEntity) {
        return ParsingHelper.getDouble(tickerStatisticsEntity.getVolume(), 0) *
                ParsingHelper.getDouble(tickerStatisticsEntity.getWeightedAvgPrice(), 0);
    }

    private static double getChange(TickerStatisticsEntity tickerStatisticsEntity) {
        return ParsingHelper.getDouble(tickerStatisticsEntity.getPriceChangePercent(), 0);
    }
}