package process.candlestick;

import client.ExchangeClient;
import component.interval.Interval;
import component.symbol.SymbolInterval;
import process.ProcessHelper;
import hibernate.EntityPersister;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerStatisticsEntity;
import helper.ParsingHelper;
import config.DaemonConfig;

import java.util.*;
import java.util.stream.Collectors;

public class CandlestickProcessHelper extends ProcessHelper {
    private static final int CANDLESTICK_LIMIT = DaemonConfig.getInstance().getCandlestickLimit();

    private final Map<SymbolInterval, List<CandlestickEntity>> cache = new HashMap<>();
    private final Map<String, Interval> stringCandlestickIntervalMap =
            Arrays.stream(Interval.values()).collect(Collectors.toMap(Interval::toString, o -> o));

    public CandlestickProcessHelper(ExchangeClient exchangeClient, EntityPersister entityPersister) {
        super(exchangeClient, entityPersister);
        loadEntitiesToCache();
    }

    private void loadEntitiesToCache() {
        entityPersister.getAllEntities(CandlestickEntity.class).forEach(this::insertToCache);
    }

    private void insertToCache(CandlestickEntity candlestickEntity){

        cache.computeIfAbsent(getSymbolCandlestickInterval(candlestickEntity), k -> new ArrayList<>()).add(candlestickEntity);
    }

    private SymbolInterval getSymbolCandlestickInterval(CandlestickEntity candlestickEntity){
        return new SymbolInterval(candlestickEntity.getSymbol(), stringCandlestickIntervalMap.get(candlestickEntity.getIntervalId()));
    }

    long getLimitedLastCloseTime(long lastCloseTime, long nowMillis, long intervalMillis) {
        return nowMillis - lastCloseTime > intervalMillis * CANDLESTICK_LIMIT ?
                getLastCloseTime(nowMillis, intervalMillis) : lastCloseTime;
    }

    private long getLastCloseTime(long nowMillis, long intervalMillis){
        return nowMillis - CANDLESTICK_LIMIT * intervalMillis;
    }

    long getLastCloseTimeFromCache(SymbolInterval symbolInterval) {
        return cache.getOrDefault(symbolInterval, new ArrayList<>()).stream()
                .filter(candlestickEntity -> candlestickEntity.getCloseTime() != null)
                .max(Comparator.comparingLong(o -> o.getCloseTime().getTime()))
                .orElse(new CandlestickEntity(){{setCloseTime(ParsingHelper.getTimestamp(0));}})
                .getCloseTime().getTime();
    }

    List<CandlestickEntity> getOldCandlesticksFromCache(SymbolInterval symbolInterval, long nowMillis, long intervalMillis) {
        long lastCloseTime = getLastCloseTime(nowMillis, intervalMillis);
        return cache.getOrDefault(symbolInterval, new ArrayList<>()).stream()
                .filter(candlestickEntity -> candlestickEntity.getCloseTime() != null && candlestickEntity.getCloseTime().getTime() < lastCloseTime)
                .collect(Collectors.toList());
    }

    void removeOldCandlesticksFromCache(SymbolInterval symbolInterval, List<CandlestickEntity> oldCandlesticks) {
        cache.getOrDefault(symbolInterval, new ArrayList<>()).removeAll(oldCandlesticks);
    }

    void addNewCandlesticksToCache(SymbolInterval symbolInterval, List<CandlestickEntity> newCandlesticks) {
        cache.computeIfAbsent(symbolInterval, k -> new ArrayList<>()).addAll(newCandlesticks);
    }

    private List<String> getSortedEnabledSymbols(List<String> enabledSymbols, List<TickerStatisticsEntity> tickerStatisticsEntities) {
        return tickerStatisticsEntities.stream()
                .sorted(TickerStatisticEntityComparator.CHANGE_PERCENT.getComparator())
                .map(TickerStatisticsEntity::getSymbol)
                .filter(enabledSymbols::contains)
                .collect(Collectors.toList());
    }

    List<CandlestickEntity> getNewCandlesticks(SymbolInterval symbolInterval, long lastCloseTime, long nowMillis) {
        return exchangeClient.getCandlestickBars(symbolInterval, CANDLESTICK_LIMIT, lastCloseTime, nowMillis);
    }

    List<String> getSortedEnabledSymbols(){
        return new ArrayList<>(getSortedEnabledSymbols(getEnabledSymbols(), exchangeClient.getTickerStatisticsEntities()));
    }
}
