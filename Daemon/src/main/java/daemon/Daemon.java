package daemon;

import client.ExchangeClient;
import component.Controllable;
import logger.LoggerProvider;
import component.Nameable;
import process.DaemonProcess;
import process.candlestick.CandlestickProcess;
import process.candlestick.CandlestickProcessHelper;
import process.candlestick.CandlestickProcessInterval;
import process.tickerprice.TickerPriceProcess;
import process.tickerprice.TickerPriceProcessHelper;
import hibernate.EntityPersister;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class Daemon implements Controllable, Nameable {
    private final EntityPersister entityPersister;
    private final ExchangeClient exchangeClient;

    private Collection<DaemonProcess> daemonProcesses = new ArrayList<>();

    public Daemon(EntityPersister entityPersister, ExchangeClient exchangeClient) {
        this.entityPersister = entityPersister;
        this.exchangeClient = exchangeClient;
    }

    @Override
    public void start(){
        if (isActive()) {
            LoggerProvider.daemon().warn(String.format("Cannot start %s. There are %d active processes", getSimpleName(), getActiveProcessesCount()));
        } else {
            LoggerProvider.daemon().info("Starting Daemon");
            daemonProcesses = getNewDaemonProcesses();
            daemonProcesses.forEach(DaemonProcess::start);
            LoggerProvider.daemon().info(String.format("%s started. Currently has %d active processes", getSimpleName(), getActiveProcessesCount()));
        }
    }

    @Override
    public void stop(boolean awaitTermination) {
        if (isActive()) {
            Collection<DaemonProcess> activeProcesses = getActiveProcesses();
            LoggerProvider.daemon().info(String.format("Stopping %s. Currently has %d active processes", getSimpleName(), activeProcesses.size()));
            stopProcessesSimultaneously(activeProcesses);
        } else {
            LoggerProvider.daemon().warn(String.format("%s is stopped. Has no active processes", getSimpleName()));
        }
    }

    @Override
    public boolean isActive() {
        return hasActiveProcess();
    }

    public long getActiveProcessesCount(){
        return daemonProcesses.stream().filter(DaemonProcess::isActive).count();
    }

    private Collection<DaemonProcess> getNewDaemonProcesses(){
        return new ArrayList<DaemonProcess>(){{
            addAll(getNewCandlestickProcesses(new CandlestickProcessHelper(exchangeClient, entityPersister)));
            add(new TickerPriceProcess(new TickerPriceProcessHelper(exchangeClient, entityPersister)));
        }};
    }

    private Collection<DaemonProcess> getNewCandlestickProcesses(CandlestickProcessHelper candlestickProcessHelper) {
        return Arrays.stream(CandlestickProcessInterval.CandlestickProcessType.values())
                .map(candlestickProcessType -> new CandlestickProcess(
                        candlestickProcessHelper,
                        CandlestickProcessInterval.getIntervalsByProcessType(candlestickProcessType),
                        candlestickProcessType.getDelayMillis()))
                .collect(Collectors.toList());
    }

    private void stopProcessesSimultaneously(Collection<DaemonProcess> activeProcesses) {
        activeProcesses.forEach(daemonProcess -> new Thread(() -> daemonProcess.stop(true)).start());
    }

    private boolean hasActiveProcess(){
        return daemonProcesses.stream().anyMatch(DaemonProcess::isActive);
    }

    private Collection<DaemonProcess> getActiveProcesses(){
        return daemonProcesses.stream().filter(DaemonProcess::isActive).collect(Collectors.toList());
    }
}
