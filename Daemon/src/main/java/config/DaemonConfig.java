package config;

import helper.ParsingHelper;

import java.util.concurrent.TimeUnit;

public class DaemonConfig extends Config{
    private static DaemonConfig instance = null;

    private DaemonConfig(){}

    public synchronized static DaemonConfig getInstance() {
        if(instance == null) {
            instance = new DaemonConfig();
        }
        return instance;
    }

    public int getCandlestickLimit(){
        return ParsingHelper.getInteger(properties.getProperty("candlestickLimit"), 10);
    }

    public long getMaxTickerPriceAgeMillis(){
        return TimeUnit.MINUTES.toMillis(ParsingHelper.getLong(properties.getProperty("maxTickerPriceAgeMinutes"), 10));
    }

    public long getTickerPriceProcessCycleDurationMillis(){
        return TimeUnit.SECONDS.toMillis(ParsingHelper.getLong(properties.getProperty("tickerPriceProcessCycleDurationSeconds"), 2));
    }

    public long getRequestTimeProcessCycleDurationMillis(){
        return TimeUnit.MINUTES.toMillis(ParsingHelper.getLong(properties.getProperty("requestTimeProcessCycleDurationMinutes"), 1));
    }

    @Override
    String getResourceName() {
        return "/daemon.properties";
    }
}
