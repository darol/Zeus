package daemon;

import client.ExchangeClient;
import client.test.TestExchangeClient;
import org.junit.Assert;
import org.junit.Test;
import process.candlestick.CandlestickProcessInterval;
import test.TestEntityPersister;

public class DaemonTest {
    private final ExchangeClient exchangeClient = new TestExchangeClient();
    private final TestEntityPersister testEntityPersister = new TestEntityPersister();
    private final Daemon daemon = new Daemon(testEntityPersister, exchangeClient);

    @Test
    public void entitySaved() throws InterruptedException {
        Assert.assertTrue(testEntityPersister.getSavedEntities().isEmpty());
        Assert.assertFalse(daemon.isActive());

        daemon.start();
        Thread.sleep(200);

        Assert.assertTrue(daemon.isActive());

        daemon.stop(false);
        Thread.sleep(200);

        Assert.assertFalse(daemon.isActive());

        int size = TestEntityPersister.getTickerPriceEntities().size();
        int length = CandlestickProcessInterval.values().length;

        Assert.assertEquals(size + length, testEntityPersister.getSavedEntities().size());
    }
}