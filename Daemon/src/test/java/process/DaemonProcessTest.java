package process;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DaemonProcessTest {
    private final long periodMillis = 200;
    private final String extraInfo = "extra info";
    private final DaemonProcess daemonProcess = new DaemonProcess() {
        @Override
        protected long getDelayMillis() {
            return periodMillis;
        }

        @Override
        protected void processCycle() {
            cycleExecuted = true;
        }

        @Override
        protected String getProcessExtraInfo() {
            return extraInfo;
        }
    };

    private boolean cycleExecuted;

    @Test
    public void getProcessExtraInfo() {
        assertEquals(extraInfo, daemonProcess.getProcessExtraInfo());
    }

    @Test
    public void processCycle() throws InterruptedException {
        cycleExecuted = false;
        daemonProcess.start();
        Thread.sleep(periodMillis);
        Assert.assertTrue(cycleExecuted);
        daemonProcess.stop(false);
    }

    @Test
    public void isActive() throws InterruptedException {
        daemonProcess.start();
        Assert.assertTrue(daemonProcess.isActive());
        daemonProcess.stop(false);
        Thread.sleep(periodMillis);
        Assert.assertFalse(daemonProcess.isActive());
    }
}