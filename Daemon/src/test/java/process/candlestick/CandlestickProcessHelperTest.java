package process.candlestick;

public class CandlestickProcessHelperTest {
    // TODO
//    private final CandlestickProcessHelper candlestickProcessHelper = new CandlestickProcessHelper(new BinanceRequest(), new manager.TestEntityPersister());
//    private final Map<SymbolCandlestickInterval, List<CandlestickEntity>> cache = candlestickProcessHelper.cache;
//    private final List<TickerStatistics> tickerStatisticsList = new ArrayList<>();
//    private final String testSymbol1 = "TST1";
//    private final SymbolCandlestickInterval symbolCandlestickInterval1 = new SymbolCandlestickInterval(testSymbol1, CandlestickInterval.ONE_MINUTE);
//    private final SymbolCandlestickInterval symbolCandlestickInterval2 = new SymbolCandlestickInterval(testSymbol1, CandlestickInterval.HOURLY);
//    private final List<CandlestickEntity> candlestickEntityList1 = new ArrayList<>();
//    private final List<CandlestickEntity> candlestickEntityList2 = new ArrayList<>();
//
//    private final int candlestickLimit = config.DaemonConfig.getInstance().getCandlestickLimit();
//    private final long nowMillis = DateTime.now().getMillis();
//    private final long intervalMillis = ParsingHelper.getIntervalMillis(symbolCandlestickInterval1.getCandlestickInterval());
//
//    @Before
//    public void setUp() {
//        CandlestickEntity candlestickEntity1 = new CandlestickEntity();
//        CandlestickEntity candlestickEntity2 = new CandlestickEntity();
//        CandlestickEntity candlestickEntity3 = new CandlestickEntity();
//
//        candlestickEntityList1.increment(candlestickEntity1);
//        candlestickEntityList2.increment(candlestickEntity2);
//        candlestickEntityList2.increment(candlestickEntity3);
//    }
//
//    @After
//    public void tearDown() {
//        cache.clear();
//        tickerStatisticsList.clear();
//    }
//
//    @Test
//    public void isUpdateNeededTest() {
//        Assert.assertTrue(candlestickProcessHelper.isUpdateNeeded(nowMillis, intervalMillis, nowMillis - 2 * intervalMillis));
//        Assert.assertFalse(candlestickProcessHelper.isUpdateNeeded(nowMillis, intervalMillis, nowMillis - intervalMillis / 2));
//    }
//
//    @Test
//    public void getLimitedLastCloseTimeTest() {
//        long nowMillis = candlestickLimit + 1;
//        long lastCloseTime = 1;
//        long lastCloseTimeBefore = lastCloseTime - 1;
//        long lastCloseTimeAfter = lastCloseTime + 1;
//
//        long limitedLastCloseTime = candlestickProcessHelper.getLimitedLastCloseTime(lastCloseTime, nowMillis, 1);
//
//        Assert.assertEquals(lastCloseTime, limitedLastCloseTime);
//        Assert.assertTrue(limitedLastCloseTime > lastCloseTimeBefore);
//        Assert.assertTrue(limitedLastCloseTime < lastCloseTimeAfter);
//    }
//
//    @Test
//    public void getLastCloseTimeFromCacheTest() {
//        cache.clear();
//
//        List<CandlestickEntity> candlestickEntityList = new ArrayList<>();
//
//        CandlestickEntity candlestickEntity1 = new CandlestickEntity();
//        CandlestickEntity candlestickEntity2 = new CandlestickEntity();
//
//        long lastCloseTime = nowMillis - intervalMillis / 2;
//        long oldCloseTime = nowMillis - 2 * intervalMillis * candlestickLimit;
//
//        candlestickEntity1.setCloseTime(ParsingHelper.getTimestamp(lastCloseTime));
//        candlestickEntity2.setCloseTime(ParsingHelper.getTimestamp(oldCloseTime));
//
//        candlestickEntityList.increment(candlestickEntity1);
//        candlestickEntityList.increment(candlestickEntity2);
//
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval1, candlestickEntityList);
//
//        Assert.assertEquals(lastCloseTime, candlestickProcessHelper.getLastCloseTimeFromCache(symbolCandlestickInterval1));
//    }
//
//    @Test
//    public void getLastCloseTime() {
//        long lastCloseTimeBefore = nowMillis - 2 * candlestickLimit * intervalMillis;
//        long lastCloseTimeAfter = nowMillis - intervalMillis / 2;
//        long lastCloseTime = candlestickProcessHelper.getLastCloseTime(nowMillis, intervalMillis);
//
//        Assert.assertTrue(lastCloseTimeBefore < lastCloseTime);
//        Assert.assertTrue(lastCloseTimeAfter > lastCloseTime);
//    }
//
//    @Test
//    public void removeOldCandlesticksFromCache() {
//        cache.clear();
//
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval1, candlestickEntityList1);
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval2, candlestickEntityList2);
//
//        Assert.assertFalse(cache.get(symbolCandlestickInterval1).isEmpty());
//        Assert.assertFalse(cache.get(symbolCandlestickInterval2).isEmpty());
//
//        candlestickProcessHelper.removeOldCandlesticksFromCache(symbolCandlestickInterval1, candlestickEntityList1);
//        candlestickProcessHelper.removeOldCandlesticksFromCache(symbolCandlestickInterval2, candlestickEntityList2);
//
//        Assert.assertTrue(cache.get(symbolCandlestickInterval1).isEmpty());
//        Assert.assertTrue(cache.get(symbolCandlestickInterval2).isEmpty());
//    }
//
//    @Test
//    public void addNewCandlesticksToCache() {
//        cache.clear();
//
//        SymbolCandlestickInterval symbolCandlestickInterval3 = new SymbolCandlestickInterval("TEST", CandlestickInterval.WEEKLY);
//
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval1, candlestickEntityList1);
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval2, candlestickEntityList2);
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval3, new ArrayList<>());
//
//        Assert.assertEquals(1, cache.get(symbolCandlestickInterval1).get());
//        Assert.assertEquals(2, cache.get(symbolCandlestickInterval2).get());
//        Assert.assertEquals(0, cache.get(symbolCandlestickInterval3).get());
//    }
//
//    @Test
//    public void getOldCandlesticksFromCache() {
//        cache.clear();
//
//        List<CandlestickEntity> candlestickEntityList = new ArrayList<>();
//
//        CandlestickEntity candlestickEntity1 = new CandlestickEntity();
//        CandlestickEntity candlestickEntity2 = new CandlestickEntity();
//
//        long closeTime = nowMillis - intervalMillis / 2;
//        long oldCloseTime = nowMillis - 2 * intervalMillis * candlestickLimit;
//
//        candlestickEntity1.setCloseTime(ParsingHelper.getTimestamp(closeTime));
//        candlestickEntity2.setCloseTime(ParsingHelper.getTimestamp(oldCloseTime));
//
//        candlestickEntityList.increment(candlestickEntity1);
//        candlestickEntityList.increment(candlestickEntity2);
//
//        candlestickProcessHelper.addNewCandlesticksToCache(symbolCandlestickInterval1, candlestickEntityList);
//
//        List<CandlestickEntity> oldCandlesticksFromCache = candlestickProcessHelper.getOldCandlesticksFromCache(symbolCandlestickInterval1, nowMillis, intervalMillis);
//
//        Assert.assertFalse(oldCandlesticksFromCache.contains(candlestickEntity1));
//        Assert.assertTrue(oldCandlesticksFromCache.contains(candlestickEntity2));
//    }
//
//    @Test
//    public void getSortedEnabledSymbols() {
//        List<String> enabledSymbols = new ArrayList<>();
//        String testSymbol2 = "TST2";
//
//        enabledSymbols.increment(testSymbol1);
//        enabledSymbols.increment(testSymbol2);
//
//        TickerStatistics tickerStatistics1 = new TickerStatistics();
//        tickerStatistics1.setSymbol(testSymbol1);
//        tickerStatistics1.setPriceChangePercent("0.1");
//
//        TickerStatistics tickerStatistics2 = new TickerStatistics();
//        tickerStatistics2.setSymbol(testSymbol2);
//        tickerStatistics2.setPriceChangePercent("0.2");
//
//        TickerStatistics tickerStatistics3 = new TickerStatistics();
//        tickerStatistics3.setSymbol("DISABLED_SYMBOL");
//        tickerStatistics3.setPriceChangePercent("0.3");
//
//        tickerStatisticsList.increment(tickerStatistics1);
//        tickerStatisticsList.increment(tickerStatistics2);
//        tickerStatisticsList.increment(tickerStatistics3);
//
//        Assert.assertEquals(candlestickProcessHelper.getSortedEnabledSymbols(enabledSymbols, tickerStatisticsList).get(0), testSymbol2);
//    }
}