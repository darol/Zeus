package process.candlestick;

import hibernate.entity.TickerStatisticsEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TickerStatisticEntityComparatorTest {
    private static final double AVG_PRICE = 0.1;
    private static final double DEF_VOLUME = 0.1;
    private static final double DEF_CHANGE = 0.1;

    @Test
    public void compare_volume() {
        TickerStatisticsEntity ts1 = new TickerStatisticsEntity();
        TickerStatisticsEntity ts2 = new TickerStatisticsEntity();

        ts1.setVolume(Double.toString(DEF_VOLUME));
        ts2.setVolume(Double.toString(DEF_VOLUME * 2));

        setAvgPrice(ts1);
        setAvgPrice(ts2);

        List<TickerStatisticsEntity> list = new ArrayList<TickerStatisticsEntity>() {{
            add(ts1);
            add(ts2);
        }};

        list.sort(TickerStatisticEntityComparator.VOLUME.getComparator());

        double v1 = Double.parseDouble(list.get(0).getVolume());
        double v2 = Double.parseDouble(list.get(1).getVolume());

        Assert.assertTrue(v1 > v2);
    }

    @Test
    public void compare_change() {
        TickerStatisticsEntity ts1 = new TickerStatisticsEntity();
        TickerStatisticsEntity ts2 = new TickerStatisticsEntity();

        ts1.setPriceChangePercent(Double.toString(DEF_CHANGE));
        ts2.setPriceChangePercent(Double.toString(DEF_CHANGE * 2));

        List<TickerStatisticsEntity> list = new ArrayList<TickerStatisticsEntity>(){{
            add(ts1);
            add(ts2);
        }};

        list.sort(TickerStatisticEntityComparator.CHANGE_PERCENT.getComparator());

        double c1 = Double.parseDouble(list.get(0).getPriceChangePercent());
        double c2 = Double.parseDouble(list.get(1).getPriceChangePercent());

        Assert.assertTrue(c1 > c2);
    }

    private void setAvgPrice(TickerStatisticsEntity tickerStatistics){
        tickerStatistics.setWeightedAvgPrice(Double.toString(AVG_PRICE));
    }
}