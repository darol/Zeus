package process;

import client.test.TestExchangeClient;
import component.symbol.SymbolEnabled;
import org.junit.Test;
import test.TestEntityPersister;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ProcessHelperTest {
    private final ProcessHelper processHelper = new ProcessHelper(new TestExchangeClient(), new TestEntityPersister()) {
        @Override
        public <T> void saveEntities(Collection<T> entities) {
            super.saveEntities(entities);
        }

        @Override
        public <T> void deleteEntities(Collection<T> entities) {
            super.deleteEntities(entities);
        }

        @Override
        protected List<String> getEnabledSymbols() {
            return super.getEnabledSymbols();
        }
    };

    @Test
    public void getEnabledSymbols() {
        List<String> enabledSymbols = processHelper.getEnabledSymbols();
        Collection<SymbolEnabled> symbolEnabledList = TestEntityPersister.getSymbolEnabledList();
        List<String> originalEnabledSymbols = symbolEnabledList.stream()
                .filter(SymbolEnabled::isEnabled)
                .map(SymbolEnabled::getSymbol)
                .collect(Collectors.toList());
        List<String> originalDisabledSymbols = symbolEnabledList.stream()
                .filter(SymbolEnabled::isDisabled)
                .map(SymbolEnabled::getSymbol)
                .collect(Collectors.toList());

        assertEquals(symbolEnabledList.stream().filter(SymbolEnabled::isEnabled).count(), enabledSymbols.size());
        originalEnabledSymbols.forEach(s -> assertTrue(enabledSymbols.contains(s)));
        originalDisabledSymbols.forEach(s -> assertFalse(enabledSymbols.contains(s)));
    }
}