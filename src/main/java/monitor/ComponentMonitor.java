package monitor;

import monitor.property.BooleanProperty;
import monitor.property.StringProperty;
import javafx.application.Platform;
import component.Process;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class ComponentMonitor extends Process {

    private void refreshProperties() {
        Arrays.stream(BooleanProperty.values()).forEach(BooleanProperty::refresh);
        Arrays.stream(StringProperty.values()).forEach(StringProperty::refresh);
    }

    @Override
    protected Runnable getRunnable() {
        return () -> Platform.runLater(this::refreshProperties);
    }

    @Override
    protected long getInitialDelayMillis() {
        return 0;
    }

    @Override
    protected long getDelayMillis() {
        return TimeUnit.SECONDS.toMillis(1);
    }
}
