package monitor;

import component.interval.Interval;
import org.joda.time.DateTime;

import java.util.Map;
import java.util.TreeMap;

public class PriceChangeMonitor {

    private final Interval interval;
    private final Map<DateTime, Double> priceHistory = new TreeMap<>();

    public PriceChangeMonitor(Interval interval) {
        this.interval = interval;
    }

    public void addPrice(double price){
        priceHistory.put(DateTime.now(), price);
    }

    public void addPrice(double price, DateTime dateTime){
        priceHistory.put(dateTime, price);
    }

    private void removeOld(){
        priceHistory.entrySet().removeIf(e -> e.getKey().isBefore(DateTime.now().minus(interval.getMillis())));
    }

    public double getChange(double currentPrice){
        removeOld();
        double historyPrice = priceHistory.values().stream().findFirst().orElse(0d);
        return historyPrice > 0 ? (currentPrice - historyPrice) * 100 / historyPrice : 0;
    }
}
