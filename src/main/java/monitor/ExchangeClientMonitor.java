package monitor;

import client.ExchangeClient;
import component.Process;

import java.util.concurrent.TimeUnit;

public class ExchangeClientMonitor extends Process {

    private final ExchangeClient exchangeClient;
    private long serverTime = 0;
    private long requestsInLastMinute = 0;

    public ExchangeClientMonitor(ExchangeClient exchangeClient) {
        this.exchangeClient = exchangeClient;
    }

    public long getServerTime() {
        return serverTime;
    }

    public long getRequestsInLastMinute(){
        return requestsInLastMinute;
    }

    @Override
    protected Runnable getRunnable() {
        return () -> {
            serverTime = exchangeClient.getServerTime();
            requestsInLastMinute = exchangeClient.getRequestsInLastMinute();
        };
    }

    @Override
    protected long getInitialDelayMillis() {
        return 0;
    }

    @Override
    protected long getDelayMillis() {
        return TimeUnit.SECONDS.toMillis(1);
    }
}
