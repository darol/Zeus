package monitor.property;

import component.interval.Interval;
import helper.ParsingHelper;
import javafx.beans.property.SimpleStringProperty;

import java.util.HashMap;
import java.util.Map;

public class PanelProperty {

    private final SimpleStringProperty price = new SimpleStringProperty();
    private final SimpleStringProperty volume = new SimpleStringProperty();
    private final Map<Interval, Change> changes = new HashMap<>();

    public SimpleStringProperty priceProperty() {
        return price;
    }

    public SimpleStringProperty volumeProperty() {
        return volume;
    }

    public SimpleStringProperty getChangeProperty(Interval interval){
        return getChange(interval).changeProperty;
    }

    public SimpleStringProperty getChangeStyleProperty(Interval interval){
        return getChange(interval).styleProperty;
    }

    public double getChangeValue(Interval interval){
        return getChange(interval).value;
    }

    private Change getChange(Interval interval){
        return changes.computeIfAbsent(interval, key -> new Change());
    }

    public double getPriceValue() {
        return ParsingHelper.getDouble(price.get(), 0);
    }

    public double getVolumeValue() {
        return ParsingHelper.getDouble(volume.get(), 0);
    }

    public void setPrice(double price) {
        this.price.set(ParsingHelper.getSignificantDigits(price));
    }

    public void setVolume(double volume) {
        this.volume.set(ParsingHelper.getSignificantDigits(volume));
    }

    public void setChangeValue(Interval interval, double value){
        getChange(interval).setFromValue(value);
    }

    private class Change{
        private SimpleStringProperty changeProperty = new SimpleStringProperty();
        private SimpleStringProperty styleProperty = new SimpleStringProperty();
        private double value = 0;

        private void setFromValue(double value){
            this.value = value;

            setStyleProperty(value);
            setChangeProperty(value);
        }

        private void setChangeProperty(double value) {
            changeProperty.set(String.format("%.1f %s", value, "%"));
        }

        private void setStyleProperty(double value) {
            styleProperty.set(value < 0 ? PropertyStyle.CHANGE_NEGATIVE.getStyle() :
                    (value > 0 ? PropertyStyle.CHANGE_POSITIVE.getStyle() : PropertyStyle.CHANGE_NONE.getStyle()));
        }
    }
}
