package monitor.property;

public enum PropertyStyle {

    CHANGE_POSITIVE("-fx-text-fill: green"),
    CHANGE_NEGATIVE("-fx-text-fill: red"),
    CHANGE_NONE("-fx-text-fill: black"),
    SLOPE_POSITIVE("-fx-fill: green"),
    SLOPE_NEGATIVE("-fx-fill: red"),
    SLOPE_NONE("-fx-fill: white");

    private final String style;

    PropertyStyle(String style) {
        this.style = style;
    }

    public String getStyle() {
        return style;
    }
}
