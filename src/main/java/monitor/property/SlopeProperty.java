package monitor.property;

import analyzer.slope.Slope;
import analyzer.slope.SlopeType;
import javafx.beans.property.SimpleStringProperty;

public class SlopeProperty {

    private Slope slope = Slope.NONE;

    private final SimpleStringProperty[] slopeStyleProperty = {
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
            new SimpleStringProperty(),
    };

    public SimpleStringProperty slopeStyleProperty(int idx) {
        return slopeStyleProperty[idx];
    }

    public void setFromSlope(Slope slope) {
        if (!this.slope.equals(slope)) {
            this.slope = slope;
            clean();
            refresh();
        }
    }

    private void clean() {
        for (int i = 0; i < slopeStyleProperty.length; i++) {
            slopeStyleProperty[i].set(PropertyStyle.SLOPE_NONE.getStyle());
        }
    }

    private void refresh() {
        for (int i = 0; i < Math.min(slopeStyleProperty.length, slope.getSize()); i++) {
            updateStyle(slope.getSlopeType(), slopeStyleProperty[i]);
        }
    }

    private void updateStyle(SlopeType slopeType, SimpleStringProperty style) {
        style.set(slopeType == SlopeType.RISING ? PropertyStyle.SLOPE_POSITIVE.getStyle() :
                (slopeType == SlopeType.FALLING ? PropertyStyle.SLOPE_NEGATIVE.getStyle() : PropertyStyle.SLOPE_NONE.getStyle()));
    }
}
