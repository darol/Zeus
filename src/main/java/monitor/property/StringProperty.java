package monitor.property;

import javafx.beans.property.SimpleStringProperty;
import helper.ParsingHelper;
import provider.ComponentProvider;

//TODO instead using this enum move that inside zeus controller
public enum StringProperty {

    DAEMON_PROCESSES_COUNT(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getDaemon().getActiveProcessesCount()));}),
    HIBERNATE_TRANSACTIONS_COUNT(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getHibernateUtil().getPerformedTransactionsCount()));}),
    ANALYZER_TICKER_CACHE_SIZE(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getMarketAnalyzer().getTickerCacheSize()));}),
    ANALYZER_CANDLE_CACHE_SIZE(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getMarketAnalyzer().getCandleCacheSize()));}),
    ANALYZER_ANALYZES_CACHE_SIZE(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getMarketAnalyzer().getSymbolAnalysesSize()));}),
    ANALYZER_LAST_ANALYSE_TIME(getNew(), object -> { object.set(ParsingHelper.getSimpleFormatDateTime(ComponentProvider.getMarketAnalyzer().getLastAnalyseMillis()));}),
    CLIENT_SERVER_TIME(getNew(), object -> { object.set(ParsingHelper.getSimpleFormatDateTime(ComponentProvider.getExchangeMonitor().getServerTime()));}),
    CLIENT_REQUESTS_IN_LAST_MINUTE(getNew(), object -> { object.set(String.valueOf(ComponentProvider.getExchangeMonitor().getRequestsInLastMinute()));});

    private final SimpleStringProperty property;
    private final RefreshableProperty<SimpleStringProperty> refreshableProperty;

    StringProperty(SimpleStringProperty property, RefreshableProperty<SimpleStringProperty> refreshableProperty) {
        this.property = property;
        this.refreshableProperty = refreshableProperty;
    }

    public SimpleStringProperty getProperty() {
        return property;
    }

    public void refresh(){
        refreshableProperty.refresh(property);
    }

    private static SimpleStringProperty getNew(){
        return new SimpleStringProperty("UNKNOWN");
    }
}
