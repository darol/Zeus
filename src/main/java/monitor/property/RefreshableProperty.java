package monitor.property;

public interface RefreshableProperty<T> {

    void refresh(T object);
}
