package monitor.property;

import javafx.beans.property.SimpleBooleanProperty;
import provider.ComponentProvider;

//TODO instead using this enum move that inside zeus controller
public enum BooleanProperty {

    HIBERNATE_STATUS(getNew(), object -> {object.set(ComponentProvider.getHibernateUtil().isActive());}),
    DAEMON_STATUS(getNew(), object -> {object.set(ComponentProvider.getDaemon().isActive());}),
    MARKET_ANALYZER_STATUS(getNew(), object -> {object.set(ComponentProvider.getMarketAnalyzer().isActive());}),
    CLIENT_STATUS(getNew(), object -> { object.set(ComponentProvider.getExchangeMonitor().getServerTime() > 0);});

    private final SimpleBooleanProperty property;
    private final RefreshableProperty<SimpleBooleanProperty> refreshableProperty;

    BooleanProperty(SimpleBooleanProperty property, RefreshableProperty<SimpleBooleanProperty> refreshableProperty) {
        this.property = property;
        this.refreshableProperty = refreshableProperty;
    }

    public SimpleBooleanProperty getProperty() {
        return property;
    }

    public void refresh(){
        refreshableProperty.refresh(property);
    }

    private static SimpleBooleanProperty getNew(){
        return new SimpleBooleanProperty(Boolean.FALSE);
    }
}