package provider;

import config.AnalyzerConfig;

public class ConfigProvider {

    private static final AnalyzerConfig ANALYZER_CONFIG = new AnalyzerConfig();

    public static AnalyzerConfig getAnalyzerConfig(){
        return ANALYZER_CONFIG;
    }
}
