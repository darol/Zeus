package provider;

import analyzer.AnalyzeProvider;
import analyzer.MarketAnalyzer;
import client.ExchangeAccountDataProvider;
import client.ExchangeClient;
import client.binance.BinanceExchangeClient;
import daemon.Daemon;
import hibernate.EntityPersister;
import hibernate.HibernateEntityPersister;
import hibernate.HibernateUtil;
import monitor.ComponentMonitor;
import monitor.ExchangeClientMonitor;

public class ComponentProvider {

    private static final EntityPersister HIBERNATE_ENTITY_PERSISTER = new HibernateEntityPersister();
    private static final ExchangeClient EXCHANGE_CLIENT = new BinanceExchangeClient();
    private static final HibernateUtil HIBERNATE_UTIL = HibernateUtil.getInstance();
    private static final Daemon DAEMON = new Daemon(HIBERNATE_ENTITY_PERSISTER, EXCHANGE_CLIENT);
    private static final MarketAnalyzer MARKET_ANALYZER = new MarketAnalyzer(HIBERNATE_ENTITY_PERSISTER);
    private static final ComponentMonitor COMPONENT_MONITOR = new ComponentMonitor();
    private static final ExchangeClientMonitor EXCHANGE_MONITOR = new ExchangeClientMonitor(EXCHANGE_CLIENT);

    public static ExchangeClientMonitor getExchangeMonitor() {
        return EXCHANGE_MONITOR;
    }

    public static ComponentMonitor getComponentMonitor() {
        return COMPONENT_MONITOR;
    }

    public static HibernateUtil getHibernateUtil() {
        return HIBERNATE_UTIL;
    }

    public static Daemon getDaemon() {
        return DAEMON;
    }

    public static MarketAnalyzer getMarketAnalyzer() {
        return MARKET_ANALYZER;
    }

    // TODO will use this for bots
    public static AnalyzeProvider getAnalyzeProvider(){
        return MARKET_ANALYZER;
    }

    // TODO will use this for bots
    public static ExchangeAccountDataProvider exchangeAccountDataProvider(){
        return EXCHANGE_CLIENT;
    }
}
