package component.helper;

import javafx.application.Platform;
import javafx.scene.control.Label;

import java.util.concurrent.TimeUnit;

public class StatusHelper {

    private final long refreshSeconds;

    public StatusHelper(long refreshSeconds) {
        this.refreshSeconds = refreshSeconds;
    }

    public void displayStatus(Label lblStatus, String statusMessage) {
        lblStatus.setText(statusMessage);

        new Thread(() -> {
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(refreshSeconds));
                Platform.runLater(() -> lblStatus.setText(""));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
