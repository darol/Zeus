package component;

import component.fxml.PanelPane;

import java.util.Comparator;

public class PanelPaneComparator {

    private final String name;
    private final Comparator<PanelPane> comparator;

    public PanelPaneComparator(String name, Comparator<PanelPane> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    public Comparator<PanelPane> getComparator() {
        return comparator;
    }

    @Override
    public String toString() {
        return name;
    }
}
