package component.fxml;

import control.fxml.SlopeController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import monitor.property.SlopeProperty;

import java.io.IOException;

public class SlopePane extends Pane {

    private final SlopeProperty slopeProperty;

    public SlopePane(SlopeProperty slopeProperty) {
        this.slopeProperty = slopeProperty;

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/analyzer/slope.fxml"));
        loader.setController(new SlopeController(slopeProperty));

        try {
            this.getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SlopeProperty getSlopeProperty() {
        return slopeProperty;
    }
}
