package component.fxml;

import analyzer.slope.Slope;
import control.fxml.PanelController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import component.interval.Interval;
import monitor.property.PanelProperty;
import monitor.property.SlopeProperty;
import provider.ConfigProvider;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PanelPane extends Pane {

    private final String symbol;
    private final PanelProperty panelProperty;
    private final Map<Interval, SlopePane> slopes = new HashMap<>();

    public PanelPane(String symbol, PanelProperty panelProperty) {
        this.symbol = symbol;
        this.panelProperty = panelProperty;

        ConfigProvider.getAnalyzerConfig().getSlopeIntervals().forEach(interval -> slopes.put(interval, new SlopePane(new SlopeProperty())));

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/analyzer/panel.fxml"));
        loader.setController(new PanelController(symbol, panelProperty, slopes.values()));

        try {
            this.getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getSymbol() {
        return symbol;
    }

    public PanelProperty getPanelProperty() {
        return panelProperty;
    }

    public void setFromSlope(Interval interval, Slope slope){
        slopes.get(interval).getSlopeProperty().setFromSlope(slope);
    }
}
