package application;

import control.fxml.LogsController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;

public class Logs extends ZeusApplication{

    private static final String LOCATION = "logs";
    private final String name;

    public Logs(String name) {
        this.name = name;
    }

    @Override
    protected String getResource() {
        return "fxml/logs.fxml";
    }

    @Override
    protected String getTitle() {
        return getSimpleName() + " - " + name;
    }

    @Override
    protected double getMinWidth() {
        return 800;
    }

    @Override
    protected double getMinHeight() {
        return 300;
    }

    @Override
    protected void onCloseRequest() {
    }

    @Override
    protected Scene getScene() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(getResource()));
        loader.setController(new LogsController(name, LOCATION));
        return new Scene(loader.load());
    }
}
