package application;

import component.Nameable;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public abstract class ZeusApplication extends Application implements Nameable {

    private final Stage primaryStage = new Stage();

    protected abstract String getResource();
    protected abstract String getTitle();
    protected abstract double getMinWidth();
    protected abstract double getMinHeight();
    protected abstract void onCloseRequest();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle(getTitle());
        primaryStage.setScene(getScene());
        primaryStage.setMinWidth(getMinWidth());
        primaryStage.setMinHeight(getMinHeight());
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> onCloseRequest());
    }

    private Parent getParent() throws IOException {
        return FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource(getResource())));
    }

    protected Scene getScene() throws IOException {
        return new Scene(getParent());
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
