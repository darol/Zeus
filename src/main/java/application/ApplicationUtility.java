package application;

import javafx.application.Application;
import javafx.stage.Stage;
import logger.LoggerProvider;

public class ApplicationUtility {

    public static void startApplication(Application application, Stage stage){
        try {
            if(!stage.isShowing()){
                application.start(stage);
            }else{
                LoggerProvider.zeus().warn(String.format("can't start %s", application.toString()));
            }
        } catch (Exception e) {
            LoggerProvider.zeus().error(String.format("Error during starting %s %s", application.toString(), e));
        }
    }

    public static void startZeusApplication(ZeusApplication zeusApplication){
        startApplication(zeusApplication, zeusApplication.getPrimaryStage());
    }
}
