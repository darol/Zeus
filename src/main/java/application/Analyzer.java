package application;

public class Analyzer extends ZeusApplication {

    @Override
    protected String getResource() {
        return "fxml/analyzer/analyzer.fxml";
    }

    @Override
    protected String getTitle() {
        return getSimpleName();
    }

    @Override
    protected double getMinWidth() {
        return 800;
    }

    @Override
    protected double getMinHeight() {
        return 600;
    }

    @Override
    protected void onCloseRequest() {

    }
}
