package application;

import logger.LoggerProvider;

public enum LogsZeusApplication {

    DAEMON(LoggerProvider.DAEMON),
    HIBERNATE(LoggerProvider.HIBERNATE),
    ANALYZER(LoggerProvider.ANALYZER),
    ZEUS(LoggerProvider.ZEUS),
    EXCHANGE(LoggerProvider.EXCHANGE);

    private final ZeusApplication zeusApplication;

    LogsZeusApplication(String name) {
        this.zeusApplication = new Logs(name);
    }

    public ZeusApplication getZeusApplication() {
        return zeusApplication;
    }
}
