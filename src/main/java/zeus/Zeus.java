package zeus;

import helper.ExecutorServiceHelper;
import javafx.application.Platform;
import javafx.scene.image.Image;
import control.ComponentController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import logger.LoggerProvider;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Zeus extends Application {

    private final ComponentController componentController = new ComponentController();

    @Override
    public void start(Stage primaryStage) throws Exception {
        LoggerProvider.zeus().info("Starting Zeus");

        primaryStage.setTitle("Zeus");
        primaryStage.setScene(getScene());
        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(600);
        primaryStage.getIcons().add(new Image("thunder.png"));
        primaryStage.show();

        componentController.startCore();

        primaryStage.setOnCloseRequest(event -> {
            LoggerProvider.zeus().info("Stopping Zeus ...");
            componentController.stopAll();
            Platform.exit();
            stopApplication();
        });
    }

    public static void main(String[] args) {
        launch();
    }

    private Parent getParent() throws IOException {
        return FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/zeus.fxml")));
    }

    private Scene getScene() throws IOException {
        return new Scene(getParent());
    }

    private void stopApplication(){
        new Thread(() -> {
            try {
                long sleepSeconds = ExecutorServiceHelper.TIMEOUT_SECONDS;
                LoggerProvider.zeus().info(String.format("Waiting %d seconds until Zeus is stopped", sleepSeconds));
                Thread.sleep(TimeUnit.SECONDS.toMillis(sleepSeconds));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LoggerProvider.zeus().info("Zeus stopped");
            System.exit(0);
        }).start();
    }
}
