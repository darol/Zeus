package config;

import component.interval.Interval;

import java.util.*;
import java.util.stream.Collectors;

public class AnalyzerConfig extends Config {

    private final Map<Type, List<Interval>> intervals = new HashMap<>();
    private static final String CHANGE_INTERVALS_KEY = "changeIntervals";
    private static final String SLOPE_INTERVALS_KEY = "slopeIntervals";
    private static final String SEPARATOR = ",";

    private enum Type{SLOPE, CHANGE}

    @Override
    String getResourceName() {
        return "/fxml/analyzer/analyzer.properties";
    }

    public List<Interval> getSlopeIntervals(){
        return intervals.computeIfAbsent(Type.SLOPE, k -> getFromString(properties.getProperty(SLOPE_INTERVALS_KEY)));
    }

    public List<Interval> getChangeIntervals(){
        return intervals.computeIfAbsent(Type.CHANGE, k -> getFromString(properties.getProperty(CHANGE_INTERVALS_KEY)));
    }

    private List<Interval> getFromString(String string){
        return Arrays.stream(string.split(SEPARATOR))
                .filter(s -> !s.isEmpty())
                .map(Interval::getFromString)
                .filter(interval -> interval != Interval.NONE)
                .collect(Collectors.toList());
    }
}
