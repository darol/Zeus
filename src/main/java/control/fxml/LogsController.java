package control.fxml;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import helper.FileHelper;
import helper.ParsingHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class LogsController implements Initializable {

    private static final int DEFAULT_LINES_LIMIT = 500;
    private final File file;

    @FXML
    private TextArea textAreaLogs;
    @FXML
    private TextField txtLinesLimit;
    @FXML
    private Label lblFileSize;

    public LogsController(String name, String location) {
        file = new File(location + "/" + name + ".log");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtLinesLimit.setText(Integer.toString(DEFAULT_LINES_LIMIT));

        refresh();
    }

    @FXML
    private void refresh(ActionEvent event) {
        refresh();
    }

    private void refresh(){
        textAreaLogs.setText(FileHelper.getContentReversed(file, ParsingHelper.getInteger(txtLinesLimit.getText(), DEFAULT_LINES_LIMIT)));
        lblFileSize.setText(String.format("%.4f KB", FileHelper.getSizeKB(file)));
    }
}
