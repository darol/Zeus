package control.fxml;

import application.ApplicationUtility;
import application.LogsZeusApplication;
import client.ExchangeName;
import application.Analyzer;
import component.helper.StatusHelper;
import control.ComponentController;
import monitor.property.BooleanProperty;
import monitor.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import provider.ComponentProvider;

import static control.Control.START;
import static control.Control.STOP;

public class ZeusController implements Initializable {

    private final Analyzer analyzer = new Analyzer();

    @FXML
    private Button btnStartDaemon;
    @FXML
    private Button btnStopDaemon;
    @FXML
    private Button btnStartAnalyzer;
    @FXML
    private Button btnStopAnalyzer;
    @FXML
    private Label labelDaemonProcesses;
    @FXML
    private Label labelCandleCacheSize;
    @FXML
    private Label labelTickerCacheSize;
    @FXML
    private Label labelExchange;
    @FXML
    private Rectangle statusHibernate;
    @FXML
    private Rectangle statusDaemon;
    @FXML
    private Rectangle statusAnalyzer;
    @FXML
    private Rectangle statusClient;
    @FXML
    private Label labelActiveBots;
    @FXML
    private Label labelServerTime;
    @FXML
    private Label labelClientRequests;
    @FXML
    private Label labelHibernateTransactions;
    @FXML
    private Label labelAnalysesCacheSize;
    @FXML
    private Label labelLastAnalyseTime;
    @FXML
    private Button btnOpenAnalyzer;
    @FXML
    private Button btnHibernateLogs;
    @FXML
    private Button btnExchangeLogs;
    @FXML
    private Button btnDaemonLogs;
    @FXML
    private Button btnAnalyzerLogs;
    @FXML
    private Label lblStatus;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        statusHibernate.disableProperty().bind(BooleanProperty.HIBERNATE_STATUS.getProperty().not());
        statusDaemon.disableProperty().bind(BooleanProperty.DAEMON_STATUS.getProperty().not());
        statusAnalyzer.disableProperty().bind(BooleanProperty.MARKET_ANALYZER_STATUS.getProperty().not());
        statusClient.disableProperty().bind(BooleanProperty.CLIENT_STATUS.getProperty().not());

        btnStartDaemon.disableProperty().bind(statusDaemon.disableProperty().not());
        btnStopDaemon.disableProperty().bind(statusDaemon.disableProperty());
        btnStartAnalyzer.disableProperty().bind(statusAnalyzer.disableProperty().not());
        btnStopAnalyzer.disableProperty().bind(statusAnalyzer.disableProperty());

        btnOpenAnalyzer.disableProperty().bind(analyzer.getPrimaryStage().showingProperty().not()
                .and(statusAnalyzer.disableProperty().not()
                .and(statusDaemon.disableProperty().not())).not());

        btnHibernateLogs.disableProperty().bind(LogsZeusApplication.HIBERNATE.getZeusApplication().getPrimaryStage().showingProperty());
        btnDaemonLogs.disableProperty().bind(LogsZeusApplication.DAEMON.getZeusApplication().getPrimaryStage().showingProperty());
        btnAnalyzerLogs.disableProperty().bind(LogsZeusApplication.ANALYZER.getZeusApplication().getPrimaryStage().showingProperty());
        btnExchangeLogs.disableProperty().bind(LogsZeusApplication.EXCHANGE.getZeusApplication().getPrimaryStage().showingProperty());

        labelDaemonProcesses.textProperty().bind(StringProperty.DAEMON_PROCESSES_COUNT.getProperty());
        labelCandleCacheSize.textProperty().bind(StringProperty.ANALYZER_CANDLE_CACHE_SIZE.getProperty());
        labelTickerCacheSize.textProperty().bind(StringProperty.ANALYZER_TICKER_CACHE_SIZE.getProperty());
        labelAnalysesCacheSize.textProperty().bind(StringProperty.ANALYZER_ANALYZES_CACHE_SIZE.getProperty());
        labelLastAnalyseTime.textProperty().bind(StringProperty.ANALYZER_LAST_ANALYSE_TIME.getProperty());
        labelServerTime.textProperty().bind(StringProperty.CLIENT_SERVER_TIME.getProperty());
        labelClientRequests.textProperty().bind(StringProperty.CLIENT_REQUESTS_IN_LAST_MINUTE.getProperty());
        labelHibernateTransactions.textProperty().bind(StringProperty.HIBERNATE_TRANSACTIONS_COUNT.getProperty());

        labelExchange.setText(ExchangeName.BINANCE.toString());
    }

    @FXML
    private void startDaemon(ActionEvent event) {
        ComponentController.control(ComponentProvider.getDaemon(), START);
    }

    @FXML
    private void stopDaemon(ActionEvent event) {
        ComponentController.control(ComponentProvider.getDaemon(), STOP);
    }

    @FXML
    private void daemonConfig(ActionEvent event) {
    }

    @FXML
    private void startAnalyzer(ActionEvent event) {
        ComponentController.control(ComponentProvider.getMarketAnalyzer(), START);
    }

    @FXML
    private void stopAnalyzer(ActionEvent event) {
        ComponentController.control(ComponentProvider.getMarketAnalyzer(), STOP);
    }

    @FXML
    private void analyzerConfig(ActionEvent event) {
    }

    @FXML
    private void daemonInfo(ActionEvent event) {
    }

    @FXML
    private void analyzerInfo(ActionEvent event) {
    }

    @FXML
    private void hibernateConfig(ActionEvent event) {
    }

    @FXML
    private void hibernateInfo(ActionEvent event) {
    }

    @FXML
    private void clientConfig(ActionEvent event) {
    }

    @FXML
    private void clientInfo(ActionEvent event) {
    }

    @FXML
    private void openAnalyzer(ActionEvent event) {
        ApplicationUtility.startApplication(analyzer, analyzer.getPrimaryStage());
    }

    @FXML
    private void hibernateLogs(ActionEvent event) {
        ApplicationUtility.startZeusApplication(LogsZeusApplication.HIBERNATE.getZeusApplication());
    }

    @FXML
    private void exchangeLogs(ActionEvent event) {
        ApplicationUtility.startZeusApplication(LogsZeusApplication.EXCHANGE.getZeusApplication());
    }

    @FXML
    private void daemonLogs(ActionEvent event) {
        ApplicationUtility.startZeusApplication(LogsZeusApplication.DAEMON.getZeusApplication());
    }

    @FXML
    private void analyzerLogs(ActionEvent event) {
        ApplicationUtility.startZeusApplication(LogsZeusApplication.ANALYZER.getZeusApplication());
    }
}
