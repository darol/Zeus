package control.fxml;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.shape.Rectangle;
import monitor.property.SlopeProperty;

import java.net.URL;
import java.util.ResourceBundle;

public class SlopeController implements Initializable {

    private final SlopeProperty slopeProperty;

    @FXML
    private Rectangle slope1;
    @FXML
    private Rectangle slope2;
    @FXML
    private Rectangle slope3;
    @FXML
    private Rectangle slope4;
    @FXML
    private Rectangle slope5;
    @FXML
    private Rectangle slope6;
    @FXML
    private Rectangle slope7;
    @FXML
    private Rectangle slope8;
    @FXML
    private Rectangle slope9;

    public SlopeController(SlopeProperty slopeProperty) {
        this.slopeProperty = slopeProperty;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        slope1.styleProperty().bind(slopeProperty.slopeStyleProperty(0));
        slope2.styleProperty().bind(slopeProperty.slopeStyleProperty(1));
        slope3.styleProperty().bind(slopeProperty.slopeStyleProperty(2));
        slope4.styleProperty().bind(slopeProperty.slopeStyleProperty(3));
        slope5.styleProperty().bind(slopeProperty.slopeStyleProperty(4));
        slope6.styleProperty().bind(slopeProperty.slopeStyleProperty(5));
        slope7.styleProperty().bind(slopeProperty.slopeStyleProperty(6));
        slope8.styleProperty().bind(slopeProperty.slopeStyleProperty(7));
        slope9.styleProperty().bind(slopeProperty.slopeStyleProperty(8));
    }
}
