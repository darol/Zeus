package control.fxml;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import analyzer.AnalyzeProvider;
import analyzer.analysis.SymbolAnalysis;
import component.PanelPaneComparator;
import component.fxml.PanelPane;
import component.helper.StatusHelper;
import helper.ParsingHelper;
import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import component.interval.Interval;
import javafx.scene.layout.HBox;
import javafx.stage.Window;
import monitor.PriceChangeMonitor;
import monitor.property.PanelProperty;
import org.joda.time.DateTime;
import provider.ComponentProvider;
import provider.ConfigProvider;

public class AnalyzerController implements Initializable {

    private static final long REFRESH_SECONDS = TimeUnit.SECONDS.toSeconds(5);
    private final StatusHelper statusHelper = new StatusHelper(REFRESH_SECONDS);
    private final AnalyzeProvider analyzeProvider = ComponentProvider.getAnalyzeProvider();
    private final ObservableList<PanelPane> panels = FXCollections.observableArrayList();
    private final ObservableList<PanelPane> filteredPanels = FXCollections.observableArrayList();
    private final List<Interval> changeIntervals = ConfigProvider.getAnalyzerConfig().getChangeIntervals();
    private final List<Interval> slopeIntervals = ConfigProvider.getAnalyzerConfig().getSlopeIntervals();
    private final Map<Interval, Map<String, PriceChangeMonitor>> monitors = changeIntervals.stream()
            .collect(Collectors.toMap(Function.identity(), v -> new HashMap<>()));

    private DateTime lastAnalysisRefresh = ParsingHelper.getUnknownDateTime();

    @FXML
    private TextField lblVolumeFilter;
    @FXML
    private ListView<PanelPane> listView;
    @FXML
    private ToggleButton btnToggleRefresh;
    @FXML
    private ChoiceBox<PanelPaneComparator> choiceBoxSort;
    @FXML
    private VBox root;
    @FXML
    private Label lblStatus;
    @FXML
    private TextField lblPriceFilter;
    @FXML
    private HBox changeRoot;
    @FXML
    private HBox slopeRoot;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listView.itemsProperty().bind(new SimpleListProperty<>(filteredPanels));

        changeIntervals.forEach(interval -> addLabel(changeRoot, interval, "label-change"));
        slopeIntervals.forEach(interval -> addLabel(slopeRoot, interval, "label-slope"));

        choiceBoxSort.setItems(FXCollections.observableArrayList(getPanelPaneComparators()));
        choiceBoxSort.getSelectionModel().selectFirst();
        choiceBoxSort.getSelectionModel().selectedIndexProperty().addListener(observable -> statusHelper.displayStatus(lblStatus,
                        "Sorting changed, will update within " + REFRESH_SECONDS + " seconds."));

        addPanels();
        refresh();
        startRefreshThread();
    }

    private void addLabel(Pane root, Interval interval, String id) {
        Label label = new Label(interval.getShortName());
        label.setId(id);
        root.getChildren().add(label);
    }

    private ArrayList<PanelPaneComparator> getPanelPaneComparators(){
        ArrayList<PanelPaneComparator> panelPaneComparators = new ArrayList<>();

        panelPaneComparators.add(new PanelPaneComparator("NONE", (o1, o2) -> 0));
        panelPaneComparators.add(new PanelPaneComparator("VOLUME_ASC", Comparator.comparingDouble(o -> o.getPanelProperty().getVolumeValue())));
        panelPaneComparators.add(new PanelPaneComparator("VOLUME_DESC", (o1, o2) -> Double.compare(o2.getPanelProperty().getVolumeValue(), o1.getPanelProperty().getVolumeValue())));

        changeIntervals.forEach(interval -> addChangeComparators(panelPaneComparators, interval));

        return panelPaneComparators;
    }

    private void addChangeComparators(ArrayList<PanelPaneComparator> panelPaneComparators, Interval interval){
        panelPaneComparators.add(new PanelPaneComparator(interval.toString() + "_ASC",
                Comparator.comparingDouble(o -> o.getPanelProperty().getChangeValue(interval))));
        panelPaneComparators.add(new PanelPaneComparator(interval.toString() + "_DESC",
                (o1, o2) -> Double.compare(o2.getPanelProperty().getChangeValue(interval), o1.getPanelProperty().getChangeValue(interval))));
    }

    private void addPanels() {
        analyzeProvider.getSymbols().forEach(this::addPanel);
    }

    private void addPanel(String symbol) {
        panels.add(new PanelPane(symbol, new PanelProperty()));
    }

    private void startRefreshThread() {
        new Thread(() -> {
            // TODO fix that as now it is creating new refreshThread every time window is opened
            while (true) {
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(REFRESH_SECONDS));
                    if (canRefresh()) {
                        Platform.runLater(this::refresh);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private boolean canRefresh() {
        Scene scene = root.getScene();
        Window window = scene != null ? scene.getWindow() : null;
        return window != null && window.isShowing() && btnToggleRefresh.isSelected();
    }

    private void refresh() {
        refreshPriceChange();
        refreshAnalysis();
        sort();
    }

    private void refreshPriceChange() {
        panels.forEach(this::refreshPriceChange);
    }

    private void refreshAnalysis() {
        if (isNewAnalysisAvailable()){
            panels.forEach(this::refreshAnalysis);
            lastAnalysisRefresh = DateTime.now();
            filter();
            statusHelper.displayStatus(lblStatus, "Analysis refreshed");
        }
    }

    private void refreshPriceChange(PanelPane panelPane) {
        String symbol = panelPane.getSymbol();
        PanelProperty panelProperty = panelPane.getPanelProperty();
        double currentPrice = analyzeProvider.getCurrentPrice(symbol);

        panelProperty.setPrice(currentPrice);

        monitors.forEach((interval, monitor) -> refreshChange(panelProperty, symbol, currentPrice, interval, monitor));
    }

    private void refreshChange(PanelProperty panelProperty, String symbol, double currentPrice, Interval interval, Map<String, PriceChangeMonitor> monitor) {
        monitor.computeIfAbsent(symbol, k -> new PriceChangeMonitor(interval)).addPrice(currentPrice);
        panelProperty.setChangeValue(interval, monitor.get(symbol).getChange(currentPrice));
    }

    private boolean isNewAnalysisAvailable() {
        return lastAnalysisRefresh.isBefore(analyzeProvider.getLastRefreshed());
    }

    private void refreshAnalysis(PanelPane panelPane) {
        SymbolAnalysis symbolAnalysis = analyzeProvider.getSymbolAnalysis(panelPane.getSymbol());
        panelPane.getPanelProperty().setVolume(symbolAnalysis.getByInterval(Interval.HOURLY).getVolume().getSumLimit(24));
        slopeIntervals.forEach(interval -> panelPane.setFromSlope(interval, symbolAnalysis.getByInterval(interval).getSlope()));
    }

    @FXML
    private void refresh(ActionEvent event) {
        statusHelper.displayStatus(lblStatus, btnToggleRefresh.isSelected() ?
                "Starting automatic refresh (refresh performed every " + REFRESH_SECONDS + " seconds)" : "Stopping automatic refresh");
    }

    @FXML
    private void filter(ActionEvent event) {
        filter();
        sort();
    }

    private void filter() {
        filteredPanels.clear();
        filteredPanels.addAll(panels.stream().filter(getFilterPredicate()).collect(Collectors.toList()));
    }

    private Predicate<PanelPane> getFilterPredicate() {
        return panelPane -> panelPane.getPanelProperty().getVolumeValue() > ParsingHelper.getLong(lblVolumeFilter.getText(), 0) &&
                panelPane.getPanelProperty().getPriceValue() > ParsingHelper.getDouble(lblPriceFilter.getText(), 0);
    }

    private void sort() {
        filteredPanels.sort(choiceBoxSort.getSelectionModel().getSelectedItem().getComparator());
    }
}
