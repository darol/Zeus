package control.fxml;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

import component.fxml.SlopePane;
import component.interval.Interval;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import monitor.property.PanelProperty;
import provider.ConfigProvider;

public class PanelController implements Initializable {

    private final String symbol;
    private final PanelProperty panelProperty;
    private final Collection<SlopePane> slopePanes;

    @FXML
    private HBox root;
    @FXML
    private Label lblSymbol;
    @FXML
    private Label lblPrice;
    @FXML
    private Label lblVolume;
    @FXML
    private HBox changeRoot;

    public PanelController(String symbol, PanelProperty panelProperty, Collection<SlopePane> slopePanes) {
        this.symbol = symbol;
        this.panelProperty = panelProperty;
        this.slopePanes = slopePanes;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.getChildren().addAll(slopePanes);

        lblSymbol.setText(symbol);

        lblPrice.textProperty().bind(panelProperty.priceProperty());
        lblVolume.textProperty().bind(panelProperty.volumeProperty());

        ConfigProvider.getAnalyzerConfig().getChangeIntervals().forEach(this::addLabel);
    }

    private void addLabel(Interval interval){
        Label label = new Label();
        label.textProperty().bind(panelProperty.getChangeProperty(interval));
        label.styleProperty().bind(panelProperty.getChangeStyleProperty(interval));
        label.setId("label-change");
        changeRoot.getChildren().add(label);
    }
}