package control;

import component.Controllable;
import logger.LoggerProvider;
import provider.ComponentProvider;

import java.util.ArrayList;
import java.util.Collection;

public class ComponentController {

    private final Collection<Controllable> controllableList = new ArrayList<>();

    public ComponentController() {
        controllableList.add(ComponentProvider.getDaemon());
        controllableList.add(ComponentProvider.getHibernateUtil());
        controllableList.add(ComponentProvider.getMarketAnalyzer());
        controllableList.add(ComponentProvider.getExchangeMonitor());
        controllableList.add(ComponentProvider.getComponentMonitor());
    }

    public void startCore(){
        control(ComponentProvider.getExchangeMonitor(), Control.START);
        control(ComponentProvider.getComponentMonitor(), Control.START);
    }

    public void stopAll(){
        LoggerProvider.zeus().info(String.format("Stopping %s controllable", controllableList.size()));
        controllableList.forEach(Controllable::stopNow);
    }

    public static void control(Controllable controllable, Control control) {
        LoggerProvider.zeus().info(String.format("Requested %s of %s controllable",
                control.toString(), controllable.getSimpleName()));
        if (control == Control.START) {
            controllable.start();
        } else if (control == Control.STOP){
            controllable.stop(true);
        }
    }
}