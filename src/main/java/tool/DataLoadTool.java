package tool;

import client.ExchangeClient;
import client.binance.BinanceExchangeClient;
import config.DataBaseConfig;
import process.candlestick.CandlestickProcessInterval;
import hibernate.EntityPersister;
import hibernate.HibernateEntityPersister;
import hibernate.entity.TickerEnabledEntity;
import hibernate.entity.TickerPriceEntity;

import java.util.ArrayList;
import java.util.List;

public class DataLoadTool {

    private static final EntityPersister ENTITY_PERSISTER = new HibernateEntityPersister();
    private static final ExchangeClient EXCHANGE_CLIENT = new BinanceExchangeClient();

    public static void main(String[] args) {
        System.out.println(CandlestickProcessInterval.getNumberOfProcesses());
//        saveEnabledTickers();
    }

    private static void saveEnabledTickers() {
        List<TickerPriceEntity> allPrices = EXCHANGE_CLIENT.getAllPrices();
        List<TickerEnabledEntity> tickerEnabledEntities = new ArrayList<>();

        for(TickerPriceEntity tickerpriceEntity : allPrices){
            if(tickerpriceEntity.getSymbol().endsWith("BTC")){
                TickerEnabledEntity tickerEnabledEntity = new TickerEnabledEntity();

                tickerEnabledEntity.setSymbol(tickerpriceEntity.getSymbol());
                tickerEnabledEntity.setEnabled(DataBaseConfig.getTrueString());

                tickerEnabledEntities.add(tickerEnabledEntity);
            }
        }
        ENTITY_PERSISTER.save(tickerEnabledEntities);
    }
}