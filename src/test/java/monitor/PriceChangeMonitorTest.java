package monitor;

import component.interval.Interval;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class PriceChangeMonitorTest {


    @Test
    public void getChange() {
        PriceChangeMonitor priceChangeMonitor = new PriceChangeMonitor(Interval.ONE_MINUTE);

        priceChangeMonitor.addPrice(1, DateTime.now().minus(TimeUnit.MILLISECONDS.toMillis(100)));
        priceChangeMonitor.addPrice(2, DateTime.now().minus(TimeUnit.MILLISECONDS.toMillis(200)));
        priceChangeMonitor.addPrice(3, DateTime.now().minus(TimeUnit.MILLISECONDS.toMillis(300)));

        Assert.assertEquals(33.3, priceChangeMonitor.getChange(4), 0.1d);
        Assert.assertEquals(-66.6, priceChangeMonitor.getChange(1), 0.1d);

        priceChangeMonitor.addPrice(5, DateTime.now().minus(TimeUnit.MINUTES.toMillis(1) + 100));

        Assert.assertEquals(33.3, priceChangeMonitor.getChange(4), 0.1d);
    }
}