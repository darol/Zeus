package parser;

import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;

public class DefaultExchangeParser implements ExchangeParser {
    @Override
    public <T> CandlestickEntity getCandlestickEntity(SymbolInterval symbolInterval, T object) {
        return new CandlestickEntity();
    }

    @Override
    public <T> TickerPriceEntity getTickerPriceEntity(T object) {
        return new TickerPriceEntity();
    }

    @Override
    public <T> TickerStatisticsEntity getTickerStatisticsEntity(T object) {
        return new TickerStatisticsEntity();
    }
}
