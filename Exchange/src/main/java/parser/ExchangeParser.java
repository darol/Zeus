package parser;

import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;

public interface ExchangeParser {
    /**
     * @param symbolInterval pair containing ticker symbol and candlestick interval
     * @param object representing CandleStick
     * @param <T> type of object representing CandleStick
     * @return CandlestickEntity
     */
    <T> CandlestickEntity getCandlestickEntity(SymbolInterval symbolInterval, T object);

    /**
     * @param object representing CandleStick
     * @param <T> type of object representing CandleStick
     * @return TickerPriceEntity
     */
    <T> TickerPriceEntity getTickerPriceEntity(T object);

    /**
     * @param object representing CandleStick
     * @param <T> type of object representing CandleStick
     * @return TickerPriceEntity
     */
    <T> TickerStatisticsEntity getTickerStatisticsEntity(T object);
}
