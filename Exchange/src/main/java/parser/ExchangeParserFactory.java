package parser;

import parser.binance.BinanceExchangeParser;
import client.ExchangeName;

import java.util.HashMap;
import java.util.Map;

public class ExchangeParserFactory {
    private static final Map<ExchangeName, ExchangeParser> exchangeParserMap = new HashMap<>();

    public static ExchangeParser getParser(ExchangeName exchangeName){
        return exchangeParserMap.computeIfAbsent(exchangeName, k -> create(exchangeName));
    }

    private static ExchangeParser create(ExchangeName exchangeName) {
        switch (exchangeName) {
            case BINANCE:
                return new BinanceExchangeParser();
            default:
                return new DefaultExchangeParser();
        }
    }
}
