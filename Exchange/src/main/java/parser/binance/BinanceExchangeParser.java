package parser.binance;

import com.google.common.primitives.Ints;
import component.interval.Interval;
import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;
import net.sealake.binance.api.client.domain.market.Candlestick;
import net.sealake.binance.api.client.domain.market.CandlestickInterval;
import net.sealake.binance.api.client.domain.market.TickerPrice;
import net.sealake.binance.api.client.domain.market.TickerStatistics;
import org.joda.time.DateTime;
import helper.ParsingHelper;
import parser.ExchangeParser;

import java.sql.Timestamp;

public class BinanceExchangeParser implements ExchangeParser {

    public static CandlestickInterval getCandlestickInterval(Interval interval){
        switch (interval){
            default:
            case ONE_MINUTE:
                return CandlestickInterval.ONE_MINUTE;
            case THREE_MINUTES:
                return CandlestickInterval.THREE_MINUTES;
            case FIVE_MINUTES:
                return CandlestickInterval.FIVE_MINUTES;
            case FIFTEEN_MINUTES:
                return CandlestickInterval.FIFTEEN_MINUTES;
            case HALF_HOURLY:
                return CandlestickInterval.HALF_HOURLY;
            case HOURLY:
                return CandlestickInterval.HOURLY;
            case TWO_HOURLY:
                return CandlestickInterval.TWO_HOURLY;
            case FOUR_HOURLY:
                return CandlestickInterval.FOUR_HOURLY;
            case SIX_HOURLY:
                return CandlestickInterval.SIX_HOURLY;
            case EIGHT_HOURLY:
                return CandlestickInterval.EIGHT_HOURLY;
            case TWELVE_HOURLY:
                return CandlestickInterval.TWELVE_HOURLY;
            case DAILY:
                return CandlestickInterval.DAILY;
            case THREE_DAILY:
                return CandlestickInterval.THREE_DAILY;
            case WEEKLY:
                return CandlestickInterval.WEEKLY;
            case MONTHLY:
                return CandlestickInterval.MONTHLY;
        }
    }

    @Override
    public <T> CandlestickEntity getCandlestickEntity(SymbolInterval symbolInterval, T object) {
        CandlestickEntity candlestickEntity = new CandlestickEntity();

        if(object instanceof Candlestick){
            Candlestick candlestick = (Candlestick) object;

            candlestickEntity.setSymbol(symbolInterval.getSymbol());
            candlestickEntity.setIntervalId(symbolInterval.getInterval().toString());
            candlestickEntity.setOpenTime(ParsingHelper.getTimestamp(candlestick.getOpenTime()));
            candlestickEntity.setOpen(candlestick.getOpen());
            candlestickEntity.setHigh(candlestick.getHigh());
            candlestickEntity.setLow(candlestick.getLow());
            candlestickEntity.setClose(candlestick.getClose());
            candlestickEntity.setVolume(candlestick.getVolume());
            candlestickEntity.setCloseTime(ParsingHelper.getTimestamp(candlestick.getCloseTime()));
            candlestickEntity.setQuoteAssetVolume(candlestick.getQuoteAssetVolume());
            candlestickEntity.setNumberOfTrades(Ints.checkedCast(candlestick.getNumberOfTrades()));
            candlestickEntity.setTakerBuyBaseAssetVolume(candlestick.getTakerBuyBaseAssetVolume());
            candlestickEntity.setTakerBuyQuoteAssetVolume(candlestick.getTakerBuyQuoteAssetVolume());
        }
        return candlestickEntity;
    }

    @Override
    public <T> TickerPriceEntity getTickerPriceEntity(T object) {
        TickerPriceEntity tickerpriceEntity = new TickerPriceEntity();

        if(object instanceof TickerPrice){
            TickerPrice tickerPrice = (TickerPrice) object;

            tickerpriceEntity.setSymbol(tickerPrice.getSymbol());
            tickerpriceEntity.setPrice(tickerPrice.getPrice());
            tickerpriceEntity.setTime(new Timestamp(DateTime.now().getMillis()));
        }
        return tickerpriceEntity;
    }

    @Override
    public <T> TickerStatisticsEntity getTickerStatisticsEntity(T object) {
        TickerStatisticsEntity tickerStatisticsEntity = new TickerStatisticsEntity();

        if(object instanceof TickerStatistics){
            TickerStatistics tickerStatistics = (TickerStatistics) object;

            tickerStatisticsEntity.setPriceChange(tickerStatistics.getPriceChange());
            tickerStatisticsEntity.setPriceChangePercent(tickerStatistics.getPriceChangePercent());
            tickerStatisticsEntity.setSymbol(tickerStatistics.getSymbol());
            tickerStatisticsEntity.setVolume(tickerStatistics.getVolume());
        }
        return tickerStatisticsEntity;
    }
}
