package client;

import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;

import java.util.List;

public interface ExchangeDataProvider {
    List<TickerStatisticsEntity> getTickerStatisticsEntities();
    List<TickerPriceEntity> getAllPrices();
    List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval);
    List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval, int limit, long start, long end);
    int getRequestsInLastMinute();
    long getServerTime();
}
