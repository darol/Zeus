package client.binance;

import client.ExchangeClient;
import client.RequestType;
import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;
import net.sealake.binance.api.client.BinanceApiClientFactory;
import net.sealake.binance.api.client.BinanceApiRestClient;
import parser.binance.BinanceExchangeParser;
import client.ExchangeName;

import java.util.List;
import java.util.stream.Collectors;

public class BinanceExchangeClient extends ExchangeClient {
    private final BinanceApiRestClient client = BinanceApiClientFactory.newInstance().newRestClient();

    public BinanceExchangeClient() {
        super(ExchangeName.BINANCE);
    }

    @Override
    public List<TickerStatisticsEntity> getTickerStatisticsEntities() {
        return request(client.getAll24HrPriceStatistics(),
                RequestType.ALL_24_HR_PRICE_STATISTICS).stream()
                .map(exchangeParser::getTickerStatisticsEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<TickerPriceEntity> getAllPrices(){
        return request(client.getAllPrices(),
                RequestType.ALL_PRICES).stream()
                .map(exchangeParser::getTickerPriceEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval) {
        return request(client.getCandlestickBars(symbolInterval.getSymbol(), BinanceExchangeParser.getCandlestickInterval(symbolInterval.getInterval())),
                RequestType.CANDLESTICK_BARS, symbolInterval.toString()).stream()
                .map(candlestick -> exchangeParser.getCandlestickEntity(symbolInterval, candlestick))
                .collect(Collectors.toList());
    }

    @Override
    public List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval, int limit, long start, long end) {
        return request(client.getCandlestickBars(symbolInterval.getSymbol(), BinanceExchangeParser.getCandlestickInterval(symbolInterval.getInterval()), limit, start, end),
                RequestType.CANDLESTICK_BARS, symbolInterval.toString()).stream()
                .map(candlestick -> exchangeParser.getCandlestickEntity(symbolInterval, candlestick))
                .collect(Collectors.toList());
    }

    @Override
    public long getServerTime() {
        return request(client.getServerTime(), RequestType.SERVER_TIME);
    }
}