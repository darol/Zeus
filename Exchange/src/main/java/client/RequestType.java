package client;

public enum RequestType {
    ALL_PRICES, CANDLESTICK_BARS, ALL_24_HR_PRICE_STATISTICS, SERVER_TIME
}
