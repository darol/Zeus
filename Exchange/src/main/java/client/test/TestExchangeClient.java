package client.test;

import client.ExchangeClient;
import client.ExchangeName;
import component.symbol.SymbolInterval;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;
import test.TestEntityPersister;

import java.util.ArrayList;
import java.util.List;

public class TestExchangeClient extends ExchangeClient {
    public TestExchangeClient() {
        super(ExchangeName.TEST);
    }

    @Override
    public List<TickerStatisticsEntity> getTickerStatisticsEntities() {
        return new ArrayList<>(TestEntityPersister.getTickerStatisticsEntities());
    }

    @Override
    public List<TickerPriceEntity> getAllPrices() {
        return new ArrayList<>(TestEntityPersister.getTickerPriceEntities());
    }

    @Override
    public List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval) {
        return new ArrayList<>(TestEntityPersister.getCandlestickEntities(symbolInterval.getInterval()));
    }

    @Override
    public List<CandlestickEntity> getCandlestickBars(SymbolInterval symbolInterval, int limit, long start, long end) {
        return new ArrayList<>(TestEntityPersister.getCandlestickEntities(symbolInterval.getInterval()));
    }

    @Override
    public long getServerTime() {
        return 0;
    }
}
