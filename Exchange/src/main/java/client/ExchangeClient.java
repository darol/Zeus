package client;

import component.EventCounter;
import logger.LoggerProvider;
import parser.ExchangeParser;
import parser.ExchangeParserFactory;

import java.util.concurrent.TimeUnit;

public abstract class ExchangeClient implements ExchangeDataProvider, ExchangeAccountDataProvider {
    protected final ExchangeParser exchangeParser;
    private final EventCounter eventCounter = new EventCounter(TimeUnit.MINUTES.toMillis(1));

    public ExchangeClient(ExchangeName exchangeName) {
        exchangeParser = ExchangeParserFactory.getParser(exchangeName);
    }

    protected <T> T request(T result, RequestType requestType){
        LoggerProvider.exchange().info(String.format("%s request", requestType.toString()));
        eventCounter.increment();
        return result;
    }

    protected <T> T request(T result, RequestType requestType, String info){
        LoggerProvider.exchange().info(String.format("%s request %s", requestType.toString(), info));
        eventCounter.increment();
        return result;
    }

    @Override
    public int getRequestsInLastMinute(){
        return eventCounter.get();
    }
}
