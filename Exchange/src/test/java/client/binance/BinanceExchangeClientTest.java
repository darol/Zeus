package client.binance;

import hibernate.entity.TickerPriceEntity;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BinanceExchangeClientTest {
    private final BinanceExchangeClient binanceExchangeClient = new BinanceExchangeClient();
    private List<TickerPriceEntity> tickerPriceEntities = new ArrayList<>();

    @Before
    public void before(){
        tickerPriceEntities = binanceExchangeClient.getAllPrices();
    }

    @Test
    public void getAllPrices() {
        assertFalse(tickerPriceEntities.isEmpty());
    }

    @Test
    public void getRequestsInLastMinute(){
        assertNotEquals(0, binanceExchangeClient.getRequestsInLastMinute());
    }

    @Test
    public void getTickerStatisticsEntities() {
    }

    @Test
    public void getCandlestickBars() {
    }
}