package helper;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CalculationHelperTest {
    private final static double DELTA = 0.001;

    @Test
    public void getAverage() {
        List<Double> doubles1 = new ArrayList<Double>() {{
            add(1.0);
            add(2.0);
            add(3.0);
            add(4.0);
        }};

        Double[] doubles2 = new Double[]{
                ParsingHelper.getDouble("0.0", 0),
                ParsingHelper.getDouble("1.0", 0),
                ParsingHelper.getDouble("2.0", 0)
        };

        Assert.assertEquals(2.5, CalculationHelper.getAverage(doubles1), DELTA);
        Assert.assertEquals(1.0, CalculationHelper.getAverage(doubles2), DELTA);
    }
}