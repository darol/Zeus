package helper;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.*;

public class ExecutorServiceHelperTest {
    private final ExecutorService executorService = Executors.newFixedThreadPool(1);
    private boolean sleepInterrupted = false;

    @Before
    public void setUp() throws Exception {
        executorService.submit(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                sleepInterrupted = true;
            }
        });
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void isActive() {
        Assert.assertTrue(ExecutorServiceHelper.isActive(executorService));
    }

    @Test
    public void shutdownExecutorService() throws InterruptedException {
        ExecutorServiceHelper.shutdownExecutorService(executorService, Logger.getLogger(ExecutorServiceHelperTest.class), false);
        Thread.sleep(100);
        Assert.assertFalse(ExecutorServiceHelper.isActive(executorService));
        Assert.assertTrue(sleepInterrupted);
    }
}