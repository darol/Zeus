package helper;

import org.junit.Assert;
import org.junit.Test;

public class ParsingHelperTest {
    private final static double DELTA = 0.001;

    // TODO increment missing test

    @Test
    public void getDouble() {
        Assert.assertEquals(4.0, ParsingHelper.getDouble("4.0", 0), DELTA);
        Assert.assertEquals(0.0, ParsingHelper.getDouble(".0", 0), DELTA);
        Assert.assertEquals(0.0, ParsingHelper.getDouble("", 0), DELTA);
        Assert.assertEquals(1.0, ParsingHelper.getDouble(" 1", 0), DELTA);
        Assert.assertEquals(-1.0, ParsingHelper.getDouble(" -1", 0), DELTA);
        Assert.assertEquals(0.0, ParsingHelper.getDouble(null, 0), DELTA);
    }

    @Test
    public void getLong() {
        Assert.assertEquals(10000000000000L, ParsingHelper.getLong("10000000000000", 0));
    }

    @Test
    public void getInteger() {
        Assert.assertEquals(-100, ParsingHelper.getInteger("-100", 0));
    }

    @Test
    public void getNowTimestamp() {
    }

    @Test
    public void getTimestamp() {
    }

    @Test
    public void getDateTime() {
    }

    @Test
    public void getLastPrice() {
    }

    @Test
    public void getSignificantDigits() {
        Assert.assertEquals("1", ParsingHelper.getSignificantDigits(1));
        Assert.assertEquals("1000", ParsingHelper.getSignificantDigits(1000));
        Assert.assertEquals("1000", ParsingHelper.getSignificantDigits(1000.123));
        Assert.assertEquals("0.100", ParsingHelper.getSignificantDigits(0.100001));
        Assert.assertEquals("0.00123", ParsingHelper.getSignificantDigits(0.00123456));
        Assert.assertEquals("0", ParsingHelper.getSignificantDigits(0.000000));

        Assert.assertEquals("-1", ParsingHelper.getSignificantDigits(-1));
        Assert.assertEquals("-1000", ParsingHelper.getSignificantDigits(-1000));
        Assert.assertEquals("-1000", ParsingHelper.getSignificantDigits(-1000.123));
        Assert.assertEquals("-0.100", ParsingHelper.getSignificantDigits(-0.100001));
        Assert.assertEquals("-0.00123", ParsingHelper.getSignificantDigits(-0.00123456));
        Assert.assertEquals("0", ParsingHelper.getSignificantDigits(-0.000000));
    }
}