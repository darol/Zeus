package component;

import helper.ParsingHelper;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

public class CacheTest {

    private static final int MAX_REFRESH_DURATION_MILLIS = 5000;
    private final String key = "key";
    private final String value = "value";

    private final Cache<String, String> stringStringCache = new Cache<String, String>() {
        @Override
        public long size() {
            return cache.size();
        }

        @Override
        protected void populate() {
            add(key, value);
        }

        @Override
        protected Logger getLogger() {
            return Logger.getLogger(CacheTest.class);
        }

        @Override
        public long getRefreshPeriodMillis() {
            return 100;
        }

        @Override
        public long getInitialDelayMillis() {
            return 0;
        }
    };

    @Test
    public void getLastRefreshed() {
        Assert.assertEquals(ParsingHelper.getUnknownDateTime(), stringStringCache.getLastRefreshed());
        stringStringCache.refresh();
        Assert.assertTrue(stringStringCache.getLastRefreshed().isAfter(DateTime.now().minusMillis(MAX_REFRESH_DURATION_MILLIS)));
    }

    @Test
    public void size() {
        stringStringCache.clear();
        Assert.assertEquals(0, stringStringCache.size());
        stringStringCache.populate();
        Assert.assertEquals(1, stringStringCache.size());
    }

    @Test
    public void get() {
        stringStringCache.clear();
        String otherValue = "otherValue";
        Assert.assertEquals(otherValue, stringStringCache.getOrDefault(key, otherValue));
        Assert.assertEquals(0, stringStringCache.getKeysCount());

        stringStringCache.populate();
        Assert.assertEquals(value, stringStringCache.getOrDefault(key, otherValue));
        Assert.assertEquals(1, stringStringCache.getKeysCount());
    }
}