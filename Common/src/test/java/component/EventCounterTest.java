package component;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class EventCounterTest {
    private final long durationMillis = TimeUnit.MILLISECONDS.toMillis(1000);
    private final EventCounter durationEventCounter = new EventCounter(durationMillis);
    private final EventCounter eventCounter = new EventCounter();

    @Test
    public void get() throws InterruptedException {
        durationEventCounter.increment();
        assertEquals(1, durationEventCounter.get());
        Thread.sleep(durationMillis);
        assertEquals(0, durationEventCounter.get());
    }

    @Test
    public void increment() throws InterruptedException {
        eventCounter.increment();
        eventCounter.increment();
        assertEquals(2, eventCounter.get());
        Thread.sleep(durationMillis);
        assertEquals(2, eventCounter.get());
    }
}