package component;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProcessTest {
    private final Process process = new Process() {
        @Override
        protected Runnable getRunnable() {
            return () -> { };
        }

        @Override
        protected long getInitialDelayMillis() {
            return 0;
        }

        @Override
        protected long getDelayMillis() {
            return 1000;
        }
    };

    @Before
    public void setUp() throws Exception {
        process.start();
    }

    @After
    public void tearDown() throws Exception {
        process.stop(false);
    }

    @Test
    public void isActive() throws InterruptedException {
        Assert.assertTrue(process.isActive());
        process.stop(false);
        Thread.sleep(100);
        Assert.assertFalse(process.isActive());
    }
}