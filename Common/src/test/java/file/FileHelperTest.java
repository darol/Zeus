package file;

import helper.FileHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class FileHelperTest {

    private File file;
    private final String[] lines = {"first\n", "second\n", "third\n"};

    @Before
    public void setUp() throws Exception {
        file = File.createTempFile("testFile", "");
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));

        for (String line : lines) {
            writer.write(line);
        }

        writer.close();
    }

    @After
    public void tearDown() throws Exception {
        if(file.exists()){
            file.delete();
        }
    }

    @Test
    public void getContentReversed() {
        String expected = "";
        int limitLines = 2;
        int counter = 0;

        for(int i = lines.length - 1; i >= 0 && counter < limitLines; i--){
            expected += lines[i];
            counter++;
        }

        Assert.assertEquals(expected, FileHelper.getContentReversed(file, limitLines));
    }

    @Test
    public void getSizeKB() {
        Assert.assertTrue(FileHelper.getSizeKB(file) > 0);
    }
}