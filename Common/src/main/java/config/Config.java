package config;

import com.google.common.io.Resources;

import java.io.IOException;
import java.util.Properties;

public abstract class Config {
    final Properties properties = loadProperties();

    private Properties loadProperties() {
        Properties properties = new Properties();
        try {
            properties.load(Resources
                    .asByteSource(Resources.getResource(this.getClass(), getResourceName()))
                    .openBufferedStream());
        } catch (final IOException ioException) {
            ioException.printStackTrace();
        }
        return properties;
    }

    abstract String getResourceName();
}
