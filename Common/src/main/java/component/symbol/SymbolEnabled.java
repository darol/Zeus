package component.symbol;

public class SymbolEnabled {
    private final String symbol;
    private final boolean enabled;

    public SymbolEnabled(String symbol, boolean enabled) {
        this.symbol = symbol;
        this.enabled = enabled;
    }

    public String getSymbol() {
        return symbol;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isDisabled(){
        return !enabled;
    }
}
