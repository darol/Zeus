package component.symbol;

import java.sql.Timestamp;
import java.util.Objects;

public class SymbolInfo {
    public static final SymbolInfo UNKNOWN = new SymbolInfo("", 0, 0, null);

    private final String symbol;
    private final double price;
    private final double volume;
    private final Timestamp timestamp;

    public SymbolInfo(String symbol, double price, double volume, Timestamp timestamp) {
        this.symbol = symbol;
        this.price = price;
        this.volume = volume;
        this.timestamp = timestamp;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getPrice() {
        return price;
    }

    public double getVolume() {
        return volume;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SymbolInfo that = (SymbolInfo) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(symbol, that.symbol) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(symbol, price, timestamp);
    }
}
