package component.symbol;

import component.interval.Interval;

import java.util.Objects;

public class SymbolInterval {
    private final String symbol;
    private final Interval interval;

    public SymbolInterval(String symbol, Interval interval) {
        this.symbol = symbol;
        this.interval = interval;
    }

    public String getSymbol() {
        return symbol;
    }

    public Interval getInterval() {
        return interval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SymbolInterval that = (SymbolInterval) o;
        return Objects.equals(symbol, that.symbol) &&
                interval == that.interval;
    }

    @Override
    public int hashCode() {

        return Objects.hash(symbol, interval);
    }

    @Override
    public String toString() {
        return "SymbolInterval{" +
                "symbol='" + symbol + '\'' +
                ", interval=" + interval +
                '}';
    }
}
