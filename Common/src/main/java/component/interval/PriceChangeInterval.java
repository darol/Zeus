package component.interval;

public enum PriceChangeInterval {
    ONE_MINUTE(Interval.ONE_MINUTE),
    FIVE_MINUTES(Interval.FIVE_MINUTES);

    private final Interval interval;

    PriceChangeInterval(Interval interval) {
        this.interval = interval;
    }

    public Interval getInterval() {
        return interval;
    }
}
