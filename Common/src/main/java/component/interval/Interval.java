package component.interval;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public enum Interval {
    NONE(Long.MAX_VALUE, "0"),
    SECOND(TimeUnit.SECONDS.toMillis(1), "1s"),
    ONE_MINUTE(TimeUnit.MINUTES.toMillis(1), "1m"),
    THREE_MINUTES(TimeUnit.MINUTES.toMillis(3), "3m"),
    FIVE_MINUTES(TimeUnit.MINUTES.toMillis(5), "5m"),
    FIFTEEN_MINUTES(TimeUnit.MINUTES.toMillis(15), "15m"),
    HALF_HOURLY(TimeUnit.MINUTES.toMillis(30), "30m"),
    HOURLY(TimeUnit.HOURS.toMillis(1), "1h"),
    TWO_HOURLY(TimeUnit.HOURS.toMillis(2), "2h"),
    FOUR_HOURLY(TimeUnit.HOURS.toMillis(4), "4h"),
    SIX_HOURLY(TimeUnit.HOURS.toMillis(6), "6h"),
    EIGHT_HOURLY(TimeUnit.HOURS.toMillis(8), "8h"),
    TWELVE_HOURLY(TimeUnit.HOURS.toMillis(12), "12h"),
    DAILY(TimeUnit.DAYS.toMillis(1), "1d"),
    THREE_DAILY(TimeUnit.DAYS.toMillis(2), "3d"),
    WEEKLY(TimeUnit.DAYS.toMillis(7), "1w"),
    MONTHLY(TimeUnit.DAYS.toMillis(30), "1m");

    private static final Map<String, Interval> STRING_INTERVAL_MAP = Arrays.stream(values()).collect(Collectors.toMap(Enum::toString, o -> o));

    private final long millis;
    private final String shortName;

    Interval(long millis, String shortName) {
        this.millis = millis;
        this.shortName = shortName;
    }

    public long getMillis() {
        return millis;
    }

    public String getShortName() {
        return shortName;
    }

    public static Interval getFromString(String s){
        return STRING_INTERVAL_MAP.getOrDefault(s, Interval.NONE);
    }
}
