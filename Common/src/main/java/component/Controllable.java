package component;

public interface Controllable extends Nameable{
    void start();
    void stop(boolean awaitTermination);
    default void stopNow(){
        stop(true);
    }
    boolean isActive();
}
