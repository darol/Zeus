package component;

import helper.ParsingHelper;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class Cache<K, V> implements Refreshable, Nameable {
    private DateTime lastRefreshed = ParsingHelper.getUnknownDateTime();
    protected final Map<K, V> cache = Collections.synchronizedMap(new HashMap<>());

    @Override
    public final DateTime getLastRefreshed() {
        return lastRefreshed;
    }

    @Override
    public final void refresh(){
        DateTime refreshStart = DateTime.now();
        long sizeBefore = size();
        synchronized (cache){
            clear();
            populate();
        }
        lastRefreshed = DateTime.now();
        logFinishedRefresh(sizeBefore, size(), refreshStart, lastRefreshed);
    }

    public abstract long size();
    protected abstract void populate();
    protected abstract Logger getLogger();

    protected void clear(){
        cache.clear();
        lastRefreshed = ParsingHelper.getUnknownDateTime();
    }

    protected void add(K key, V value){
        cache.put(key, value);
    }

    protected V getOrDefault(K key, V defaultValue){
        return cache.getOrDefault(key, defaultValue);
    }

    protected int getKeysCount(){
        return cache.keySet().size();
    }

    private void logFinishedRefresh(long sizeBefore, long sizeAfter, DateTime refreshStart, DateTime refreshEnd){
        getLogger().info(String.format("Finished refreshing %s cache in %d millis. Size before: %d, after: %d",
                getSimpleName(), refreshEnd.getMillis() - refreshStart.getMillis(), sizeBefore, sizeAfter));
    }

}
