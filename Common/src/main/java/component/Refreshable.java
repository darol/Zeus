package component;

import org.joda.time.DateTime;

public interface Refreshable {
    void refresh();
    long getRefreshPeriodMillis();
    long getInitialDelayMillis();
    DateTime getLastRefreshed();
}
