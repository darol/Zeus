package component;

public interface Nameable {
    default String getSimpleName(){
        return this.getClass().getSimpleName();
    }
    default String getName(){
        return this.getClass().getName();
    }
    default String getTypeName(){
        return this.getClass().getTypeName();
    }
}
