package component;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.HashSet;

public class EventCounter {
    private final long durationMillis;
    private final Collection<DateTime> dateTimes = new HashSet<>();
    private final boolean hasDuration;

    private int counter = 0;

    public EventCounter(long durationMillis) {
        this.durationMillis = durationMillis;
        hasDuration = hasDuration();
    }

    public EventCounter(){
        durationMillis = 0;
        hasDuration = hasDuration();
    }

    public int get(){
        return hasDuration ? removeOldAndGetSize() : counter;
    }

    public synchronized void increment(){
        if(hasDuration){
            dateTimes.add(DateTime.now());
        }else{
            counter ++;
        }
    }

    private synchronized int removeOldAndGetSize(){
        DateTime now = DateTime.now();
        dateTimes.removeIf(dateTime -> dateTime.isBefore(now.minus(durationMillis)));
        return dateTimes.size();
    }

    private boolean hasDuration(){
        return durationMillis > 0;
    }
}
