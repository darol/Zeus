package component;

import helper.ExecutorServiceHelper;
import logger.LoggerProvider;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class Process implements Controllable {
    private static final int DEFAULT_POOL_SIZE = 1;
    private ScheduledExecutorService scheduledExecutorService = null;

    protected abstract Runnable getRunnable();
    protected abstract long getInitialDelayMillis();
    protected abstract long getDelayMillis();

    @Override
    public void start() {
        scheduledExecutorService = Executors.newScheduledThreadPool(DEFAULT_POOL_SIZE);
        scheduledExecutorService.scheduleAtFixedRate(getRunnable(), getInitialDelayMillis(), getDelayMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public void stop(boolean awaitTermination) {
        ExecutorServiceHelper.shutdownExecutorService(scheduledExecutorService, LoggerProvider.zeus(), awaitTermination);
    }

    @Override
    public boolean isActive() {
        return ExecutorServiceHelper.isActive(scheduledExecutorService);
    }
}
