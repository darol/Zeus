package logger;

import org.apache.log4j.Logger;

public class LoggerProvider {
    public static final String EXCHANGE = "exchange";
    public static final String DAEMON = "daemon";
    public static final String ZEUS = "zeus";
    public static final String ANALYZER = "analyzer";
    public static final String HIBERNATE = "hibernate";

    public static Logger daemon(){
        return Logger.getLogger(DAEMON);
    }

    public static Logger exchange(){
        return Logger.getLogger(EXCHANGE);
    }

    public static Logger zeus(){
        return Logger.getLogger(ZEUS);
    }

    public static Logger analyzer() {
        return Logger.getLogger(ANALYZER);
    }

    public static Logger hibernate() {
        return Logger.getLogger(HIBERNATE);
    }
}