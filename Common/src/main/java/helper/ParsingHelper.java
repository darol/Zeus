package helper;

import component.interval.Interval;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.stream.IntStream;

public class ParsingHelper {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private static final String unknownTimeString = "UNKNOWN";
    private static final DateTime UNKNOWN_DATE_TIME = new DateTime(0);
    private static final int SIGNIFICANT_DIGITS = 3;

    public static double getDouble(String string, double defaultValue){
        try{
            return Double.parseDouble(string);
        }catch (Exception e){
            return defaultValue;
        }
    }

    public static long getLong(String string, long defaultValue){
        try{
            return Long.parseLong(string);
        }catch (Exception e){
            return defaultValue;
        }
    }

    public static int getInteger(String string, int defaultValue){
        try{
            return Integer.parseInt(string);
        }catch (Exception e){
            return defaultValue;
        }
    }

    public static String getSimpleFormatDateTime(long millis){
        return millis > 0 ? simpleDateFormat.format(getTimestamp(millis)) : unknownTimeString;
    }

    public static Timestamp getNowTimestamp(){
        return new Timestamp(DateTime.now().getMillis());
    }

    public static Timestamp getPastTimeStamp(Interval interval){
        Timestamp timestamp = getNowTimestamp();
        timestamp.setTime(timestamp.getTime() - interval.getMillis());
        return timestamp;
    }

    public static Timestamp getTimestamp(DateTime dateTime){
        return new Timestamp(dateTime.getMillis());
    }

    public static Timestamp getTimestamp(String stringMillis){
        return new Timestamp(getLong(stringMillis, 0));
    }

    public static Timestamp getTimestamp(long millis){
        return new Timestamp(millis);
    }

    public static DateTime getDateTime(Timestamp timestamp){
        return new DateTime(timestamp.getTime());
    }

    public static DateTime getUnknownDateTime() {
        return UNKNOWN_DATE_TIME;
    }

    public static String getSignificantDigits(double value){
        StringBuilder stringBuilder = new StringBuilder();
        String stringValue = String.format("%.8f", value);

        int digits = 0;
        int idx = 0;
        boolean match = false;
        boolean negative = value < 0;

        value = Math.abs(value);

        if(value < 1){
            while (idx < stringValue.length()){
                char c = stringValue.charAt(idx++);
                stringBuilder.append(c);
                if(Character.isDigit(c) && Character.getNumericValue(c) > 0){
                    match = true;
                }
                if(match){
                    digits++;
                    if(digits >= SIGNIFICANT_DIGITS){
                        break;
                    }
                }
            }
            return value == 0 ? "0" : stringBuilder.toString();
        }else{
            return (negative ? "-" : "") + (value == 0 ? "0" : Long.toString((long) value));
        }


    }
}