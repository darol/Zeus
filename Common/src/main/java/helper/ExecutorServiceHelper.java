package helper;

import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceHelper {
    public final static long TIMEOUT_SECONDS = TimeUnit.SECONDS.toSeconds(2);

    public static boolean isActive(ExecutorService executorService){
        return executorService != null && !executorService.isShutdown() && !executorService.isTerminated();
    }

    public static void shutdownExecutorService(ExecutorService executorService, Logger logger, boolean awaitTermination){
        if(isActive(executorService)){
            new Thread(() -> shutdownExecutor(executorService, logger, awaitTermination)).start();
        }
    }

    private static void shutdownExecutor(ExecutorService executorService, Logger logger, boolean awaitTermination) {
        try {
            shutdown(executorService, logger);
            awaitTermination(executorService, logger, awaitTermination);
            shutdownNow(executorService, logger);
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private static void shutdown(ExecutorService executorService, Logger logger) {
        logger.info("Shutting down executor");
        executorService.shutdown();
    }

    private static void awaitTermination(ExecutorService executorService, Logger logger, boolean awaitTermination) throws InterruptedException {
        if(awaitTermination && !executorService.isTerminated()){
            logger.info(String.format("Awaiting %s seconds for executor termination", TIMEOUT_SECONDS));
            executorService.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
    }

    private static void shutdownNow(ExecutorService executorService, Logger logger) {
        if(!executorService.isTerminated()){
            logger.info("Executor not terminated, shut down now");
            executorService.shutdownNow();
        }else{
            logger.info("Executor shut down");
        }
    }
}