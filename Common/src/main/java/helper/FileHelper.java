package helper;

import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.File;
import java.io.IOException;

public class FileHelper {

    public static String getContentReversed(File file){
        return getContentReversed(file, Integer.MAX_VALUE);
    }

    public static String getContentReversed(File file, int limitLines){
        StringBuilder stringBuilder = new StringBuilder();

        try {
            ReversedLinesFileReader reversedLinesFileReader = new ReversedLinesFileReader(file);
            String line;
            int lines = 0;
            while ((line = reversedLinesFileReader.readLine()) != null && lines < limitLines){
                stringBuilder.append(line).append("\n");
                lines ++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public static double getSizeKB(File file){
        return file.length() / 1024d;
    }
}
