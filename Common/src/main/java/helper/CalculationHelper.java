package helper;

import java.util.Arrays;
import java.util.List;

public class CalculationHelper {
    public static double getAverage(Double... doubles){
        return getAverage(Arrays.asList(doubles));
    }

    public static double getAverage(List<Double> values){
        return values.stream().mapToDouble(value -> value).average().orElse(0);
    }
}
