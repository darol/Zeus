package test;

import component.interval.Interval;
import component.symbol.SymbolEnabled;
import config.DataBaseConfig;
import hibernate.EntityPersister;
import hibernate.entity.CandlestickEntity;
import hibernate.entity.TickerEnabledEntity;
import hibernate.entity.TickerPriceEntity;
import hibernate.entity.TickerStatisticsEntity;
import org.joda.time.DateTime;
import helper.ParsingHelper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class TestEntityPersister implements EntityPersister {
    public static final String SYMBOL = "SYMBOL";
    public static final double TICKER_CURRENT_PRICE = 0.0070;
    private static final Collection<SymbolEnabled> symbolEnabledList = createSymbolEnabledList();
    private static Collection<TickerPriceEntity> tickerPriceEntities = createTickerPriceEntities();
    private static Collection<CandlestickEntity> candlestickEntities = createCandlestickEntities();
    private static Collection<TickerStatisticsEntity> tickerStatisticsEntities = createTickerStatisticEntities();

    private final Collection<Object> savedEntities = new ArrayList<>();
    private final Collection<Object> deletedEntities = new ArrayList<>();

    @Override
    public <T> Collection<T> getAllEntities(Class<T> entityClass) {
        if (entityClass.getSimpleName().equals(TickerEnabledEntity.class.getSimpleName())) {
            return getTickerEnabledEntities().stream()
                    .filter(entityClass::isInstance)
                    .map(entityClass::cast)
                    .collect(Collectors.toList());
        } else if (entityClass.getSimpleName().equals(TickerPriceEntity.class.getSimpleName())) {
            return getTickerPriceEntities().stream()
                    .filter(entityClass::isInstance)
                    .map(entityClass::cast)
                    .collect(Collectors.toList());
        } else if (entityClass.getSimpleName().equals(CandlestickEntity.class.getSimpleName())) {
            return getCandlestickEntities().stream()
                    .filter(entityClass::isInstance)
                    .map(entityClass::cast)
                    .collect(Collectors.toList());
        } else if (entityClass.getSimpleName().equals(TickerStatisticsEntity.class.getSimpleName())) {
            return getTickerStatisticsEntities().stream()
                    .filter(entityClass::isInstance)
                    .map(entityClass::cast)
                    .collect(Collectors.toList());
        }  else {
            return new ArrayList<>();
        }
    }

    @Override
    public <T> void delete(Collection<T> entities) {
        deletedEntities.addAll(entities);
    }

    @Override
    public <T> void delete(T entity) {
        deletedEntities.add(entity);
    }

    @Override
    public <T> void save(Collection<T> entities) {
        savedEntities.addAll(entities);
    }

    @Override
    public <T> void save(T entity) {
        savedEntities.add(entity);
    }

    public Collection<Object> getSavedEntities() {
        return savedEntities;
    }

    public Collection<Object> getDeletedEntities() {
        return deletedEntities;
    }

    public static Collection<TickerPriceEntity> getTickerPriceEntities(){
        return tickerPriceEntities;
    }

    public static Collection<TickerStatisticsEntity> getTickerStatisticsEntities() {
        return tickerStatisticsEntities;
    }

    public static Collection<CandlestickEntity> getCandlestickEntities() {
        return candlestickEntities;
    }

    public static Collection<CandlestickEntity> getCandlestickEntities(Interval interval) {
        Collection<CandlestickEntity> candlestickEntities = new ArrayList<>();
        CandlestickEntity candlestickEntity = new CandlestickEntity();
        candlestickEntity.setIntervalId(interval.toString());
        candlestickEntities.add(candlestickEntity);
        return candlestickEntities;
    }

    public static Collection<SymbolEnabled> getSymbolEnabledList() {
        return symbolEnabledList;
    }

    private static ArrayList<SymbolEnabled> createSymbolEnabledList() {
        return new ArrayList<SymbolEnabled>(){{
            add(new SymbolEnabled(SYMBOL, true));
            add(new SymbolEnabled(SYMBOL + "1", false));
            add(new SymbolEnabled(SYMBOL + "2", true));
            add(new SymbolEnabled(SYMBOL + "3", false));
        }};
    }

    private List<TickerEnabledEntity> getTickerEnabledEntities(){
        return new ArrayList<TickerEnabledEntity>(){{
            symbolEnabledList.stream()
                    .filter(SymbolEnabled::isEnabled)
                    .forEach(symbolEnabled -> add(getTickerEnabledEntity(symbolEnabled.getSymbol())));
        }};
    }

    private TickerEnabledEntity getTickerEnabledEntity(String symbol){
        TickerEnabledEntity tickerEnabledEntity = new TickerEnabledEntity();
        tickerEnabledEntity.setSymbol(symbol);
        tickerEnabledEntity.setEnabled(DataBaseConfig.getTrueString());
        return tickerEnabledEntity;
    }


    private static Collection<TickerStatisticsEntity> createTickerStatisticEntities() {
        Collection<TickerStatisticsEntity> tickerStatisticsEntities = new ArrayList<>();

        TickerStatisticsEntity tickerStatisticsEntity = new TickerStatisticsEntity();
        tickerStatisticsEntity.setSymbol(SYMBOL);
        tickerStatisticsEntities.add(tickerStatisticsEntity);

        return tickerStatisticsEntities;
    }

    static private Collection<TickerPriceEntity> createTickerPriceEntities() {
        Collection<TickerPriceEntity> tickerPriceEntities = new ArrayList<>();

        TickerPriceEntity tickerPriceEntity1 = new TickerPriceEntity();
        TickerPriceEntity tickerPriceEntity2 = new TickerPriceEntity();
        TickerPriceEntity tickerPriceEntity3 = new TickerPriceEntity();
        TickerPriceEntity tickerPriceEntity4 = new TickerPriceEntity();

        DateTime dateTime = DateTime.now();
        Timestamp old = ParsingHelper.getNowTimestamp();
        old.setTime(old.getTime() - TimeUnit.MINUTES.toMillis(1));

        tickerPriceEntity1.setSymbol(SYMBOL);
        tickerPriceEntity1.setPrice(String.valueOf(TICKER_CURRENT_PRICE));
        tickerPriceEntity1.setTime(ParsingHelper.getTimestamp(dateTime));

        tickerPriceEntity2.setSymbol(SYMBOL);
        tickerPriceEntity2.setPrice(String.valueOf("0.0210"));
        tickerPriceEntity2.setTime(ParsingHelper.getTimestamp(dateTime.minusMillis(1000)));

        tickerPriceEntity3.setSymbol(SYMBOL);
        tickerPriceEntity3.setPrice(String.valueOf("0.0119"));
        tickerPriceEntity3.setTime(ParsingHelper.getTimestamp(dateTime.minusMillis(2000)));

        tickerPriceEntity4.setSymbol(SYMBOL);
        tickerPriceEntity4.setPrice(String.valueOf("0.0021"));
        tickerPriceEntity4.setTime(ParsingHelper.getTimestamp(dateTime.minusMillis(3000)));

        tickerPriceEntities.add(tickerPriceEntity1);
        tickerPriceEntities.add(tickerPriceEntity2);
        tickerPriceEntities.add(tickerPriceEntity3);
        tickerPriceEntities.add(tickerPriceEntity4);

        return tickerPriceEntities;
    }

    private static Collection<CandlestickEntity> createCandlestickEntities(){
        Collection<CandlestickEntity> candlestickEntities = new ArrayList<>();

        CandlestickEntity candlestickEntity1 = new CandlestickEntity();
        CandlestickEntity candlestickEntity2 = new CandlestickEntity();
        CandlestickEntity candlestickEntity3 = new CandlestickEntity();

        candlestickEntity1.setSymbol(SYMBOL);
        candlestickEntity2.setSymbol(SYMBOL);
        candlestickEntity3.setSymbol(SYMBOL);

        candlestickEntity1.setIntervalId(Interval.FIVE_MINUTES.toString());
        candlestickEntity2.setIntervalId(Interval.FIVE_MINUTES.toString());
        candlestickEntity3.setIntervalId(Interval.HOURLY.toString());

        candlestickEntities.add(candlestickEntity1);
        candlestickEntities.add(candlestickEntity2);
        candlestickEntities.add(candlestickEntity3);

        return candlestickEntities;
    }
}
