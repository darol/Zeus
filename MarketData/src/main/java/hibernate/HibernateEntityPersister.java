package hibernate;

import java.util.Collection;

public class HibernateEntityPersister implements EntityPersister {
    @Override
    public <T> Collection<T> getAllEntities(Class<T> entityClass){
        return HibernateUtil.getInstance().getAllEntities(entityClass);
    }

    @Override
    public <T> void delete(Collection<T> entities){
        HibernateUtil.getInstance().deleteEntities(entities);
    }

    @Override
    public <T> void delete(T entity) {
        HibernateUtil.getInstance().deleteEntity(entity);
    }

    @Override
    public <T> void save(Collection<T> entities){
        HibernateUtil.getInstance().saveEntities(entities);
    }

    @Override
    public <T> void save(T entity) {
        HibernateUtil.getInstance().saveEntity(entity);
    }
}
