package hibernate.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ticker_statistics", schema = "binance", catalog = "")
public class TickerStatisticsEntity {
    private String symbol;
    private String priceChange;
    private String priceChangePercent;
    private String volume;
    private String weightedAvgPrice;

    @Id
    @Column(name = "symbol")
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Basic
    @Column(name = "price_change")
    public String getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(String priceChange) {
        this.priceChange = priceChange;
    }

    @Basic
    @Column(name = "price_change_percent")
    public String getPriceChangePercent() {
        return priceChangePercent;
    }

    public void setPriceChangePercent(String priceChangePercent) {
        this.priceChangePercent = priceChangePercent;
    }

    @Basic
    @Column(name = "volume")
    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    @Basic
    @Column(name = "weighted_avg_price")
    public String getWeightedAvgPrice() {
        return weightedAvgPrice;
    }

    public void setWeightedAvgPrice(String weightedAvgPrice) {
        this.weightedAvgPrice = weightedAvgPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TickerStatisticsEntity that = (TickerStatisticsEntity) o;
        return Objects.equals(symbol, that.symbol) &&
                Objects.equals(priceChange, that.priceChange) &&
                Objects.equals(priceChangePercent, that.priceChangePercent) &&
                Objects.equals(volume, that.volume) &&
                Objects.equals(weightedAvgPrice, that.weightedAvgPrice);
    }

    @Override
    public int hashCode() {

        return Objects.hash(symbol, priceChange, priceChangePercent, volume, weightedAvgPrice);
    }
}
