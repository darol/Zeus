package hibernate.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ticker_enabled", schema = "binance", catalog = "")
public class TickerEnabledEntity {
    private String symbol;
    private String enabled;

    @Id
    @Column(name = "symbol")
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Basic
    @Column(name = "enabled")
    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TickerEnabledEntity that = (TickerEnabledEntity) o;
        return Objects.equals(symbol, that.symbol) &&
                Objects.equals(enabled, that.enabled);
    }

    @Override
    public int hashCode() {

        return Objects.hash(symbol, enabled);
    }
}
