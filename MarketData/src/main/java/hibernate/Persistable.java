package hibernate;

import org.hibernate.Session;

public interface Persistable {
    void persist(Session session, Object entity);
}
