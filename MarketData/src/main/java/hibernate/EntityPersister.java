package hibernate;

import java.util.Collection;

public interface EntityPersister {
    <T> Collection<T> getAllEntities(Class<T> entityClass);
    <T> void delete(Collection<T> entities);
    <T> void delete(T entity);
    <T> void save(Collection<T> entities);
    <T> void save(T entity);
}
