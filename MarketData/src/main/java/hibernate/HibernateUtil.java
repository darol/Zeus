package hibernate;

import component.EventCounter;
import component.Nameable;
import config.CredentialsConfig;
import component.Controllable;
import logger.LoggerProvider;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.criteria.CriteriaQuery;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class HibernateUtil implements Controllable, Nameable {
    private static final SessionFactory SESSION_FACTORY = getSessionFactory();
    private static final int MAX_STOP_RETRY_COUNT = 5;
    // TODO use some global timeout like in ExecutorHelper
    private static final long TRANSACTION_STOP_RETRY_DELAY = TimeUnit.MILLISECONDS.toMillis(500);
    private static HibernateUtil instance = null;

    private final ThreadLocal<Session> threadLocalSession = new ThreadLocal<>();
    private final Map<TransactionType, Persistable> persistenceProviderMap = new HashMap<>();
    private final EventCounter eventCounter = new EventCounter(TimeUnit.HOURS.toMillis(1));
    private final EventCounter stopEventCounter = new EventCounter();

    private boolean transactionFinished = false;

    @Override
    public void start() {
        LoggerProvider.hibernate().warn("Attempt to start singleton instance!");
    }

    @Override
    public void stop(boolean awaitTermination) {
        if (transactionFinished) {
            stop();
        } else {
            stopEventCounter.increment();
            if (stopEventCounter.get() > MAX_STOP_RETRY_COUNT) {
                LoggerProvider.hibernate().warn(String.format("Active transaction failed to finish after %s retries", MAX_STOP_RETRY_COUNT));
                stop();
            } else {
                LoggerProvider.hibernate().warn("Cannot stop now because there is an active transaction, retrying ...");
                new Thread(() -> {
                    try {
                        Thread.sleep(TRANSACTION_STOP_RETRY_DELAY);
                        stop(true);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }
    }

    private void stop() {
        LoggerProvider.hibernate().info(String.format("Stopping %s ...", getSimpleName()));
        closeSession();
        closeFactory();
        LoggerProvider.hibernate().info(String.format("%s stopped", getSimpleName()));
    }

    @Override
    public boolean isActive() {
        return SESSION_FACTORY != null && SESSION_FACTORY.isOpen();
    }

    private enum TransactionType {SAVE, UPDATE, SAVE_OR_UPDATE, DELETE}

    private HibernateUtil() {
        persistenceProviderMap.put(TransactionType.SAVE, Session::save);
        persistenceProviderMap.put(TransactionType.SAVE_OR_UPDATE, Session::saveOrUpdate);
        persistenceProviderMap.put(TransactionType.DELETE, Session::delete);
        persistenceProviderMap.put(TransactionType.UPDATE, Session::update);
    }

    public synchronized static HibernateUtil getInstance() {
        if (instance == null) {
            instance = new HibernateUtil();
        }
        return instance;
    }

    private static SessionFactory getSessionFactory() {
        try {
            Configuration configuration = new Configuration().configure();

            configuration.setProperty("hibernate.connection.username", CredentialsConfig.getInstance().getDbUser());
            configuration.setProperty("hibernate.connection.password", CredentialsConfig.getInstance().getDbPassword());

            return configuration.buildSessionFactory();
        } catch (Throwable ex) {
            LoggerProvider.hibernate().error(ex.getMessage(), ex);
            return null;
        }
    }

    private Session getSession() throws HibernateException {
        Session session = threadLocalSession.get();
        if (session == null || !session.isOpen()) {
            threadLocalSession.set(SESSION_FACTORY.openSession());
        }
        return threadLocalSession.get();
    }

    public void closeSession() throws HibernateException {
        Session session = threadLocalSession.get();
        if (session != null) {
            LoggerProvider.hibernate().info("Closing session.");
            session.close();
        }
        threadLocalSession.set(null);
    }

    public void closeFactory() {
        LoggerProvider.hibernate().info("Closing SESSION_FACTORY");
        SESSION_FACTORY.close();
    }

    public <T> List<T> getAllEntities(Class<T> clazz) {
        CriteriaQuery<T> criteria = getSession().getCriteriaBuilder().createQuery(clazz);
        criteria.select(criteria.from(clazz));
        return getSession().createQuery(criteria).getResultList();
    }

    public <T> void saveEntity(T entity) {
        performTransaction(new ArrayList<T>() {{
            add(entity);
        }}, TransactionType.SAVE);
    }

    public <T> void updateEntity(T entity) {
        performTransaction(new ArrayList<T>() {{
            add(entity);
        }}, TransactionType.UPDATE);
    }

    public <T> void saveOrUpdateEntity(T entity) {
        performTransaction(new ArrayList<T>() {{
            add(entity);
        }}, TransactionType.SAVE_OR_UPDATE);
    }

    public <T> void deleteEntity(T entity) {
        performTransaction(new ArrayList<T>() {{
            add(entity);
        }}, TransactionType.DELETE);
    }

    public <T> void saveEntities(Collection<T> entities) {
        performTransaction(entities, TransactionType.SAVE);
    }

    public <T> void updateEntities(Collection<T> entities) {
        performTransaction(entities, TransactionType.UPDATE);
    }

    public <T> void saveOrUpdateEntities(Collection<T> entities) {
        performTransaction(entities, TransactionType.SAVE_OR_UPDATE);
    }

    public <T> void deleteEntities(Collection<T> entities) {
        performTransaction(entities, TransactionType.DELETE);
    }

    private <T> void performTransaction(Collection<T> entities, TransactionType type) {
        if (!entities.isEmpty() && isActive()) {
            transactionFinished = false;
            Transaction tx = null;
            try {
                Session session = getSession();
                tx = session.getTransaction();
                tx.begin();

                Persistable provider = persistenceProviderMap.get(type);
                entities.forEach(entity -> provider.persist(session, entity));

                tx.commit();
                eventCounter.increment();
                LoggerProvider.hibernate().debug(String.format("Performed transaction %s on %d entities", type.toString(), entities.size()));
            } catch (Exception e) {
                LoggerProvider.hibernate().error(e);
                if (tx != null) {
                    LoggerProvider.hibernate().warn(String.format("Rolling back transaction %s", tx.toString()));
                    tx.rollback();
                }
            } finally {
                transactionFinished = true;
            }
        }
    }

    public int getPerformedTransactionsCount() {
        return eventCounter.get();
    }
}
