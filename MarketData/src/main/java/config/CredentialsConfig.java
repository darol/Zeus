package config;

public class CredentialsConfig extends Config {
    private static CredentialsConfig instance = null;

    private CredentialsConfig(){}

    public synchronized static CredentialsConfig getInstance() {
        if(instance == null) {
            instance = new CredentialsConfig();
        }
        return instance;
    }

    public String getDbUser(){
        return properties.getProperty("dbuser", "user");
    }

    public String getDbPassword(){
        return properties.getProperty("dbpassword", "password");
    }

    @Override
    String getResourceName() {
        return "/credentials.properties";
    }
}