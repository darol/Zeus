package analyzer;

import analyzer.analysis.SymbolAnalysis;
import org.joda.time.DateTime;

import java.util.Collection;

public interface AnalyzeProvider {
    double getCurrentPrice(String symbol);
    SymbolAnalysis getSymbolAnalysis(String symbol);
    Collection<String> getSymbols();
    DateTime getLastRefreshed();
}
