package analyzer;

import component.symbol.SymbolInfo;

import java.util.List;

public abstract class Analyzer<T> {
    public abstract T get(List<SymbolInfo> symbolInfos);
}
