package analyzer;

import analyzer.analysis.Analysis;
import analyzer.analysis.SymbolAnalysis;
import analyzer.slope.SlopeAnalyzer;
import analyzer.price.PriceAnalyzer;
import analyzer.volume.VolumeAnalyzer;
import cache.*;
import cache.entity.CandlestickEntityCache;
import cache.entity.TickerPriceEntityCache;
import component.Refreshable;
import component.interval.Interval;
import component.symbol.SymbolInterval;
import component.Controllable;
import helper.ExecutorServiceHelper;
import hibernate.EntityPersister;
import logger.LoggerProvider;
import component.symbol.SymbolInfo;
import component.Nameable;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class MarketAnalyzer implements Controllable, Nameable, AnalyzeProvider {

    private final SlopeAnalyzer slopeAnalyzer = new SlopeAnalyzer();
    private final VolumeAnalyzer volumeAnalyzer = new VolumeAnalyzer();
    private final PriceAnalyzer priceAnalyzer = new PriceAnalyzer();
    private final TickerPriceEntityCache tickerPriceCache;
    private final CandlestickEntityCache candlestickCache;
    private final SymbolAnalyses symbolAnalyses;
    private final Collection<Refreshable> refreshableHashSet = new HashSet<>();
    private final Map<String, SymbolInterval> symbolIntervalSecondMap = new HashMap<>();

    private ScheduledExecutorService scheduledExecutorService;

    public MarketAnalyzer(EntityPersister entityPersister) {
        tickerPriceCache = new TickerPriceEntityCache(entityPersister);
        candlestickCache = new CandlestickEntityCache(entityPersister);
        symbolAnalyses = new SymbolAnalyses();

        refreshableHashSet.add(tickerPriceCache);
        refreshableHashSet.add(candlestickCache);
        refreshableHashSet.add(symbolAnalyses);
    }

    @Override
    public void start(){
        if (isActive()) {
            LoggerProvider.analyzer().warn(String.format("Attempting to start %s, but it is already running!", getSimpleName()));
        } else {
            LoggerProvider.analyzer().info(String.format("Starting %s", getSimpleName()));
            scheduledExecutorService = Executors.newScheduledThreadPool(refreshableHashSet.size());
            refreshableHashSet.forEach(refreshable -> scheduleCacheRefresh(scheduledExecutorService, refreshable));
        }
    }

    @Override
    public void stop(boolean awaitTermination) {
        if(isActive()){
            LoggerProvider.analyzer().info(String.format("Stopping %s", getSimpleName()));
            ExecutorServiceHelper.shutdownExecutorService(scheduledExecutorService, LoggerProvider.analyzer(), awaitTermination);
        }else {
            LoggerProvider.analyzer().info(String.format("%s stopped", getSimpleName()));
        }
    }

    @Override
    public boolean isActive(){
        return ExecutorServiceHelper.isActive(scheduledExecutorService);
    }

    @Override
    public double getCurrentPrice(String symbol){
        return tickerPriceCache.getLatestSymbolInfo(getCachedSymbolInterval(symbol)).getPrice();
    }

    @Override
    public SymbolAnalysis getSymbolAnalysis(String symbol){
        return symbolAnalyses.get(symbol);
    }

    @Override
    public Collection<String> getSymbols() {
        return tickerPriceCache.getSymbols();
    }

    @Override
    public DateTime getLastRefreshed() {
        return symbolAnalyses.getLastRefreshed();
    }

    private SymbolInterval getCachedSymbolInterval(String symbol){
        return symbolIntervalSecondMap.computeIfAbsent(symbol, key -> new SymbolInterval(key, Interval.SECOND));
    }

    public long getTickerCacheSize(){
        return tickerPriceCache.size();
    }

    public long getCandleCacheSize(){
        return candlestickCache.size();
    }

    public long getSymbolAnalysesSize(){
        return symbolAnalyses.size();
    }

    public long getLastAnalyseMillis(){
        return symbolAnalyses.getLastRefreshed().getMillis();
    }

    private List<SymbolAnalysis> getSymbolAnalyses() {
        return getSymbols().stream()
                .map(this::createSymbolAnalysis).collect(Collectors.toList());
    }

    private SymbolAnalysis createSymbolAnalysis(String symbol) {
        return new SymbolAnalysis(symbol, getAnalysisList(getSymbolIntervalListMap(symbol)));
    }

    private List<Analysis> getAnalysisList(HashMap<SymbolInterval, List<SymbolInfo>> symbolIntervalListMap){
        return symbolIntervalListMap.entrySet().stream()
                .map(symbolIntervalListEntry -> getAnalysis(symbolIntervalListEntry.getKey().getInterval(), symbolIntervalListEntry.getValue()))
                .collect(Collectors.toList());
    }

    private Analysis getAnalysis(Interval interval, List<SymbolInfo> symbolInfos){
        return new Analysis(interval,
                slopeAnalyzer.get(symbolInfos),
                priceAnalyzer.get(symbolInfos),
                volumeAnalyzer.get(symbolInfos));
    }

    private void scheduleCacheRefresh(ScheduledExecutorService scheduledExecutorService, Refreshable refreshable) {
        scheduledExecutorService.scheduleAtFixedRate(
                refreshable::refresh, refreshable.getInitialDelayMillis(), refreshable.getRefreshPeriodMillis(), TimeUnit.MILLISECONDS);
    }

    private HashMap<SymbolInterval, List<SymbolInfo>> getSymbolIntervalListMap(String symbol) {
        return new HashMap<SymbolInterval, List<SymbolInfo>>(){{
            putAll(tickerPriceCache.get(symbol));
            putAll(candlestickCache.get(symbol));
        }};
    }

    private class SymbolAnalyses extends SymbolAnalysisCache{

        @Override
        protected Collection<SymbolAnalysis> getSymbolAnalyses() {
            return MarketAnalyzer.this.getSymbolAnalyses();
        }

        @Override
        protected Logger getLogger() {
            return LoggerProvider.analyzer();
        }
    }
}