package analyzer;

import component.symbol.SymbolInfo;

import java.util.List;
import java.util.stream.Collectors;

public class AnalyzerHelper {
    public static List<Double> getPrices(List<SymbolInfo> symbolInfos) {
        return symbolInfos.stream().map(SymbolInfo::getPrice).collect(Collectors.toList());
    }
}
