package analyzer.slope;

import analyzer.Analyzer;
import analyzer.AnalyzerHelper;
import component.symbol.SymbolInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SlopeAnalyzer extends Analyzer<Slope> {
    @Override
    public Slope get(List<SymbolInfo> symbolInfos) {
        List<Double> prices = AnalyzerHelper.getPrices(symbolInfos);
        return prices.size() > 1 ? getSlope(prices) : getSlopeNone(prices);
    }

    private Slope getSlope(List<Double> prices){
        Collections.reverse(prices);

        List<Double> doubles = new ArrayList<>();
        Iterator<Double> pricesIterator = prices.iterator();
        double currentPrice = pricesIterator.next();
        double previousPrice = pricesIterator.next();
        SlopeType slopeType = getSlopeType(currentPrice, previousPrice);

        doubles.add(currentPrice);
        doubles.add(previousPrice);

        while (pricesIterator.hasNext()){
            previousPrice = currentPrice;
            currentPrice = pricesIterator.next();

            if(canAdd(slopeType, currentPrice, previousPrice)){
                doubles.add(currentPrice);
            }else{
                break;
            }
        }

        Collections.reverse(doubles);

        return new Slope(slopeType, doubles);
    }

    private boolean canAdd(SlopeType slopeType, double currentPrice, double previousPrice){
        return slopeType == SlopeType.NONE ||
                (slopeType == SlopeType.FALLING ? currentPrice > previousPrice :
                (slopeType == SlopeType.RISING && currentPrice < previousPrice));
    }

    private SlopeType getSlopeType(double currentPrice, double previousPrice){
        return currentPrice > previousPrice ? SlopeType.RISING :
                (currentPrice < previousPrice ? SlopeType.FALLING : SlopeType.NONE);
    }

    private Slope getSlopeNone(List<Double> prices){
        return new Slope(SlopeType.NONE, prices);
    }
}
