package analyzer.slope;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Slope {
    public static final Slope NONE = new Slope(SlopeType.NONE, new ArrayList<>());
    private final SlopeType slopeType;
    private final List<Double> values;

    public Slope(SlopeType slopeType, List<Double> values) {
        this.slopeType = slopeType;
        this.values = values;
    }

    public int getSize() {
        return values.size();
    }

    public SlopeType getSlopeType() {
        return slopeType;
    }

    public List<Double> getValues() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slope slope = (Slope) o;
        return slopeType == slope.slopeType &&
                Objects.equals(values, slope.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slopeType, values);
    }

    @Override
    public String toString() {
        return "Slope{" +
                "slopeType=" + slopeType +
                ", values=" + values +
                '}';
    }
}
