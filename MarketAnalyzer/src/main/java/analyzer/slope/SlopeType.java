package analyzer.slope;

public enum SlopeType {
    NONE, RISING, FALLING
}
