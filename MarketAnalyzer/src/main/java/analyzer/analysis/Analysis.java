package analyzer.analysis;

import analyzer.price.Price;
import analyzer.slope.Slope;
import analyzer.volume.Volume;
import component.interval.Interval;

public class Analysis {
    public static final Analysis NONE = new Analysis(Interval.NONE, Slope.NONE, Price.NONE, Volume.NONE);
    private final Interval interval;
    private final Slope slope;
    private final Price price;
    private final Volume volume;

    public Analysis(Interval interval, Slope slope, Price price, Volume volume) {
        this.interval = interval;
        this.slope = slope;
        this.price = price;
        this.volume = volume;
    }

    public Interval getInterval() {
        return interval;
    }

    public Slope getSlope() {
        return slope;
    }

    public Price getPrice() {
        return price;
    }

    public Volume getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        return "Analysis{" +
                "interval=" + interval +
                ", slope=" + slope +
                ", price=" + price +
                ", volume=" + volume +
                '}';
    }
}
