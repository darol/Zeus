package analyzer.analysis;

import component.interval.Interval;

import java.util.Comparator;
import java.util.List;

public class SymbolAnalysis {
    private final String symbol;
    private final List<Analysis> analysisList;

    public SymbolAnalysis(String symbol, List<Analysis> analysisList) {
        this.symbol = symbol;
        this.analysisList = analysisList;
        this.analysisList.sort(Comparator.comparing(Analysis::getInterval));
    }

    public String getSymbol() {
        return symbol;
    }

    public List<Analysis> getAnalysisList() {
        return analysisList;
    }

    public int size(){
        return analysisList.size();
    }

    public Analysis getByInterval(Interval interval){
        return analysisList.stream().filter(analyze -> analyze.getInterval().equals(interval)).findFirst().orElse(Analysis.NONE);
    }

    @Override
    public String toString() {
        return "SymbolAnalysis{" +
                "symbol='" + symbol + '\'' +
                ", analysisList=" + analysisList +
                '}';
    }
}
