package analyzer.volume;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Volume {
    public static final Volume NONE = new Volume(new ArrayList<>());
    private final List<Double> values;

    public Volume(List<Double> values) {
        this.values = values;
        Collections.reverse(values);
    }

    public double getSum(){
        return values.stream().mapToDouble(value -> value).sum();
    }

    public double getSumLimit(int limit){
        return values.stream().limit(limit).mapToDouble(value -> value).sum();
    }
}
