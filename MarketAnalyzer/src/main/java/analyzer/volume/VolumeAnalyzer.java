package analyzer.volume;

import analyzer.Analyzer;
import component.symbol.SymbolInfo;

import java.util.List;
import java.util.stream.Collectors;

public class VolumeAnalyzer extends Analyzer<Volume> {
    @Override
    public Volume get(List<SymbolInfo> symbolInfos) {
        return new Volume(symbolInfos.stream().map(SymbolInfo::getVolume).collect(Collectors.toList()));
    }
}
