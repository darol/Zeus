package analyzer.price;

import analyzer.Analyzer;
import analyzer.AnalyzerHelper;
import component.symbol.SymbolInfo;

import java.util.Comparator;
import java.util.List;

public class PriceAnalyzer extends Analyzer<Price> {
    @Override
    public Price get(List<SymbolInfo> symbolInfos) {
        List<Double> prices = AnalyzerHelper.getPrices(symbolInfos);

        return new Price(symbolInfos.stream().mapToDouble(SymbolInfo::getPrice).average().orElse(0),
                prices.stream().min(Double::compareTo).orElse(0d),
                prices.stream().max(Comparator.naturalOrder()).orElse(0d));
    }
}
