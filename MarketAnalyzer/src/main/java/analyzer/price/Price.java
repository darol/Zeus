package analyzer.price;

public class Price {
    public static final Price NONE = new Price(0,0,0);
    private final double diff;
    private final double avg;
    private final double min;
    private final double max;

    public Price(double avg, double min, double max) {
        this.diff = max - min;
        this.avg = avg;
        this.min = min;
        this.max = max;
    }

    public double getDiff() {
        return diff;
    }

    public double getAvg() {
        return avg;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    @Override
    public String toString() {
        return "Price{" +
                "diff=" + diff +
                '}';
    }
}
