package cache.entity;

import com.google.common.collect.Lists;
import component.Cache;
import component.symbol.SymbolInterval;
import hibernate.EntityPersister;
import component.symbol.SymbolInfo;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public abstract class EntityCache<E> extends Cache<SymbolInterval, List<SymbolInfo>> {
    private final Class<E> entityClass;
    private final EntityPersister entityPersister;

    EntityCache(Class<E> entityClass, EntityPersister entityPersister) {
        this.entityClass = entityClass;
        this.entityPersister = entityPersister;
    }

    protected abstract SymbolInfo getSymbolInfo(E e);
    protected abstract SymbolInterval getSymbolInterval(E e);

    public SymbolInfo getLatestSymbolInfo(SymbolInterval symbolInterval){
        return cache.getOrDefault(symbolInterval, new ArrayList<>()).stream()
                .max(Comparator.comparing(SymbolInfo::getTimestamp))
                .orElse(SymbolInfo.UNKNOWN);
    }

    public Map<SymbolInterval, List<SymbolInfo>> get(String symbol){
        return cache.entrySet().stream()
                .filter(symbolIntervalListEntry -> symbolIntervalListEntry.getKey().getSymbol().equals(symbol))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public Collection<String> getSymbols(){
        return cache.keySet().stream()
                .map(SymbolInterval::getSymbol).distinct()
                .collect(Collectors.toList());
    }

    @Override
    protected void populate() {
        entityPersister.getAllEntities(entityClass).forEach(this::addToCache);
    }

    private void addToCache(E e){
        SymbolInterval symbolInterval = getSymbolInterval(e);
        List<SymbolInfo> list = getOrDefault(symbolInterval, Collections.synchronizedList(new ArrayList<>()));
        list.add(getSymbolInfo(e));
        add(symbolInterval, list);
    }

    @Override
    public long size(){
        return cache.values().stream()
                .mapToInt(List::size)
                .sum();
    }
}
