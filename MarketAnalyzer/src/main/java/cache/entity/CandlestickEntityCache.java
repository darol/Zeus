package cache.entity;

import helper.CalculationHelper;
import logger.LoggerProvider;
import component.interval.Interval;
import component.symbol.SymbolInterval;
import hibernate.EntityPersister;
import hibernate.entity.CandlestickEntity;
import component.symbol.SymbolInfo;
import helper.ParsingHelper;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class CandlestickEntityCache extends EntityCache<CandlestickEntity> {

    public CandlestickEntityCache(EntityPersister entityPersister) {
        super(CandlestickEntity.class, entityPersister);
    }

    @Override
    public long getRefreshPeriodMillis() {
        return TimeUnit.MINUTES.toMillis(5);
    }

    @Override
    public long getInitialDelayMillis() {
        return 0;
    }

    @Override
    protected SymbolInfo getSymbolInfo(CandlestickEntity candlestickEntity) {
        return new SymbolInfo(candlestickEntity.getSymbol(),
                CalculationHelper.getAverage(
                        ParsingHelper.getDouble(candlestickEntity.getOpen(), 0),
                        ParsingHelper.getDouble(candlestickEntity.getClose(), 0)),
                ParsingHelper.getDouble(candlestickEntity.getQuoteAssetVolume(), 0),
                candlestickEntity.getCloseTime());
    }

    @Override
    protected SymbolInterval getSymbolInterval(CandlestickEntity candlestickEntity) {
        return new SymbolInterval(candlestickEntity.getSymbol(), Interval.getFromString(candlestickEntity.getIntervalId()));
    }

    @Override
    protected Logger getLogger() {
        return LoggerProvider.analyzer();
    }
}
