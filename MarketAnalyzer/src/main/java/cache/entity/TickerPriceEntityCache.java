package cache.entity;

import logger.LoggerProvider;
import component.interval.Interval;
import component.symbol.SymbolInterval;
import hibernate.EntityPersister;
import hibernate.entity.TickerPriceEntity;
import component.symbol.SymbolInfo;
import helper.ParsingHelper;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class TickerPriceEntityCache extends EntityCache<TickerPriceEntity> {
    private final Interval currentPriceInterval = Interval.SECOND;

    public TickerPriceEntityCache(EntityPersister entityPersister) {
        super(TickerPriceEntity.class, entityPersister);
    }

    @Override
    public long getRefreshPeriodMillis() {
        return TimeUnit.SECONDS.toMillis(5);
    }

    @Override
    public long getInitialDelayMillis() {
        return 0;
    }

    @Override
    protected SymbolInfo getSymbolInfo(TickerPriceEntity tickerPriceEntity) {
        return new SymbolInfo(tickerPriceEntity.getSymbol(),
                ParsingHelper.getDouble(tickerPriceEntity.getPrice(), 0),
                0,
                tickerPriceEntity.getTime());
    }

    @Override
    protected SymbolInterval getSymbolInterval(TickerPriceEntity tickerPriceEntity) {
        return new SymbolInterval(tickerPriceEntity.getSymbol(), currentPriceInterval);
    }

    @Override
    protected Logger getLogger() {
        return LoggerProvider.analyzer();
    }
}
