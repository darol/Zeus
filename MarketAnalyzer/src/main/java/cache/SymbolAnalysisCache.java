package cache;

import analyzer.analysis.SymbolAnalysis;
import component.Cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

public abstract class SymbolAnalysisCache extends Cache<String, SymbolAnalysis> {
    public static final long INITIAL_DELAY_MILLIS = 200;

    @Override
    public long getInitialDelayMillis() {
        return TimeUnit.MILLISECONDS.toMillis(INITIAL_DELAY_MILLIS);
    }

    @Override
    public long getRefreshPeriodMillis() {
        return TimeUnit.MINUTES.toMillis(1);
    }

    public SymbolAnalysis get(String symbol){
        return getOrDefault(symbol, new SymbolAnalysis(symbol, new ArrayList<>()));
    }

    protected abstract Collection<SymbolAnalysis> getSymbolAnalyses();

    @Override
    protected void populate() {
        Collection<SymbolAnalysis> symbolAnalyses = getSymbolAnalyses();
        symbolAnalyses.forEach(symbolAnalysis -> add(symbolAnalysis.getSymbol(), symbolAnalysis));
    }

    @Override
    public long size() {
        return getKeysCount();
    }
}