package analyzer.slope;

import component.symbol.SymbolInfo;
import org.junit.Test;
import helper.ParsingHelper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static test.TestEntityPersister.SYMBOL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SlopeAnalyzerTest {

    @Test
    public void get() {
        SlopeAnalyzer slopeAnalyzer = new SlopeAnalyzer();
        List<SymbolInfo> symbolInfos = new ArrayList<>();
        Timestamp timestamp = ParsingHelper.getNowTimestamp();

        Slope slope = slopeAnalyzer.get(symbolInfos);

        assertEquals(SlopeType.NONE, slope.getSlopeType());
        assertTrue(slope.getValues().isEmpty());

        symbolInfos.clear();
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.02, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.03, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.04, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.05, 0, timestamp));

        slope = slopeAnalyzer.get(symbolInfos);

        assertEquals(SlopeType.RISING, slope.getSlopeType());
        assertEquals(5, slope.getSize());

        symbolInfos.clear();
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.02, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.03, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.02, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));

        slope = slopeAnalyzer.get(symbolInfos);

        assertEquals(SlopeType.FALLING, slope.getSlopeType());
        assertEquals(3, slope.getSize());

        symbolInfos.clear();
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));

        slope = slopeAnalyzer.get(symbolInfos);

        assertEquals(SlopeType.NONE, slope.getSlopeType());
        assertEquals(3, slope.getSize());
    }
}