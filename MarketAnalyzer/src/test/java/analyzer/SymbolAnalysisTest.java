package analyzer;

import analyzer.analysis.Analysis;
import analyzer.analysis.SymbolAnalysis;
import analyzer.price.Price;
import analyzer.slope.Slope;
import analyzer.volume.Volume;
import component.interval.Interval;
import org.junit.Before;
import org.junit.Test;
import test.TestEntityPersister;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class SymbolAnalysisTest {
    private final Analysis ANALYSIS_FIVE_MIN_NONE = new Analysis(Interval.FIVE_MINUTES, Slope.NONE, Price.NONE, Volume.NONE);
    private final Analysis ANALYSIS_HOURLY_NONE = new Analysis(Interval.HOURLY, Slope.NONE, Price.NONE, Volume.NONE);

    private SymbolAnalysis symbolAnalysis;

    @Before
    public void setUp() {
        symbolAnalysis = new SymbolAnalysis(TestEntityPersister.SYMBOL, new ArrayList<Analysis>(){{
            add(ANALYSIS_FIVE_MIN_NONE);
            add(ANALYSIS_HOURLY_NONE);
        }});
    }

    @Test
    public void size() {
        assertEquals(2, symbolAnalysis.size());
    }

    @Test
    public void getByInterval() {
        assertEquals(ANALYSIS_FIVE_MIN_NONE, symbolAnalysis.getByInterval(Interval.FIVE_MINUTES));
        assertEquals(Analysis.NONE, symbolAnalysis.getByInterval(Interval.MONTHLY));
    }
}