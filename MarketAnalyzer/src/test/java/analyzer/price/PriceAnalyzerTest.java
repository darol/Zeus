package analyzer.price;

import component.symbol.SymbolInfo;
import org.junit.Test;
import helper.ParsingHelper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static test.TestEntityPersister.SYMBOL;


public class PriceAnalyzerTest {

    @Test
    public void get() {
        PriceAnalyzer priceAnalyzer = new PriceAnalyzer();
        List<SymbolInfo> symbolInfos = new ArrayList<>();
        Timestamp timestamp = ParsingHelper.getNowTimestamp();

        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.02, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.03, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.04, 0, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.05, 0, timestamp));

        Price price = priceAnalyzer.get(symbolInfos);

        double delta = 0.0001;
        assertEquals(0.04, price.getDiff(), delta);
        assertEquals(0.03, price.getAvg(), delta);
        assertEquals(0.01, price.getMin(), delta);
        assertEquals(0.05, price.getMax(), delta);
    }
}
