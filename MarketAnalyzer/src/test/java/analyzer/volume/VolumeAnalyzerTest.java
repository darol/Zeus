package analyzer.volume;

import component.symbol.SymbolInfo;
import org.junit.Test;
import helper.ParsingHelper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static test.TestEntityPersister.SYMBOL;
import static org.junit.Assert.assertEquals;

public class VolumeAnalyzerTest {

    @Test
    public void get() {
        VolumeAnalyzer volumeAnalyzer = new VolumeAnalyzer();
        List<SymbolInfo> symbolInfos = new ArrayList<>();
        Timestamp timestamp = ParsingHelper.getNowTimestamp();

        symbolInfos.add(new SymbolInfo(SYMBOL, 0.01, 5, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.02, 10, timestamp));
        symbolInfos.add(new SymbolInfo(SYMBOL, 0.03, 20, timestamp));

        assertEquals(35, volumeAnalyzer.get(symbolInfos).getSum(), 0.1);
    }
}