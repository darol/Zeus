package analyzer.volume;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class VolumeTest {
    List<Double> doubleList = new ArrayList<Double>(){{
        add(1d);
        add(2d);
        add(3d);
        add(4d);
    }};


    @Test
    public void getSum() {
        Assert.assertEquals(10d, new Volume(doubleList).getSum(), 0.1);
    }

    @Test
    public void getSumLimit() {
        Assert.assertEquals(7d, new Volume(doubleList).getSumLimit(2), 0.1);
    }
}