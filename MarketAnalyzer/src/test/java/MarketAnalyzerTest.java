
import analyzer.MarketAnalyzer;
import analyzer.analysis.SymbolAnalysis;
import cache.SymbolAnalysisCache;
import component.interval.Interval;
import org.junit.*;
import test.TestEntityPersister;

import static org.junit.Assert.*;

public class MarketAnalyzerTest {
    private static final long SET_UP_DELAY_MILLIS = 200;
    private static final MarketAnalyzer MARKET_ANALYZER = new MarketAnalyzer(new TestEntityPersister());

    @BeforeClass
    public static void setUp() throws Exception {
        MARKET_ANALYZER.start();
        // Wait before caches are loaded
        Thread.sleep(SymbolAnalysisCache.INITIAL_DELAY_MILLIS + SET_UP_DELAY_MILLIS);
    }

    @AfterClass
    public static void tearDown() {
        MARKET_ANALYZER.stop(false);
    }

    @Test
    public void getCurrentPrice() {
        assertEquals(TestEntityPersister.TICKER_CURRENT_PRICE, MARKET_ANALYZER.getCurrentPrice(TestEntityPersister.SYMBOL), 0.0001);
    }

    @Test
    public void getSymbolAnalysis() {
        SymbolAnalysis symbolAnalysis = MARKET_ANALYZER.getSymbolAnalysis(TestEntityPersister.SYMBOL);

        assertEquals(3, symbolAnalysis.size());
        assertEquals(Interval.SECOND, symbolAnalysis.getAnalysisList().get(0).getInterval());
        assertEquals(Interval.FIVE_MINUTES, symbolAnalysis.getAnalysisList().get(1).getInterval());
        assertEquals(Interval.HOURLY, symbolAnalysis.getAnalysisList().get(2).getInterval());
    }
}